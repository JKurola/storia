from flask import Flask
from flask.ext.login import LoginManager
import os
from config import WIKISTORIA_URL

app = Flask(__name__)

app.debug = True

from app.database import db_session

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

app.config['MAX_CONTENT_LENGTH'] = 1 * 1024 * 1024

# added for user login/logout
app.config.from_object("config")
lm = LoginManager()
lm.init_app(app)

if os.getenv("VCAP_SERVICES"):
    lm.login_view = WIKISTORIA_URL
else:
    lm.login_view = 'http://127.0.0.1:5000/'

# Register blueprints
from app.blueprints.main_views import main_page
from app.blueprints.event_datasource import event_datasource
from app.blueprints.user_management import user_page
from app.blueprints.playlist_datasource import playlist_datasource
from app.blueprints.category_datasource import category_datasource
from app.blueprints.layer_datasource import layer_datasource
app.register_blueprint(main_page)
app.register_blueprint(user_page, url_prefix='/users')
app.register_blueprint(event_datasource, url_prefix='/events')
app.register_blueprint(layer_datasource, url_prefix='/layer')
app.register_blueprint(playlist_datasource, url_prefix='/playlists')
app.register_blueprint(category_datasource, url_prefix='/categories')