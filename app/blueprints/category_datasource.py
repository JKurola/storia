# Blueprint for everything related with categories

from flask import Blueprint, request
from app.models.querymachine import QueryMachine
from flask.ext.login import login_required
from config import preset_private_categories,preset_public_categories
import json

category_datasource = Blueprint('category_datasource', __name__, template_folder='templates')

@category_datasource.route('/search/json', methods=['GET', 'POST'])
def category_search():
    searchText = request.args.get('searchText','')
    if len(searchText) > 2:
        searchResults = QueryMachine.category_search(searchText)
        return json.dumps(map(lambda x: x.serializeJSON, searchResults))
    else:
        return json.dumps([])

@category_datasource.route('/add', methods=['GET','POST'])
@login_required
def category_add():
    addText = request.form['addText']
    addDesc = request.form['addDesc']
    if len(addText) > 2:
        QueryMachine.createCategory(
            title=addText.lower().capitalize(),
            description=addDesc,
            filename="",
            eventcounter=0
        )
    return "ok"

# A remote procedure call for installing the preset topics. Check if the
# categories exist already.
@category_datasource.route('/populate/presets')
def category_install_presets():
    combined_categories = dict()

    combined_categories.update(preset_public_categories)
    combined_categories.update(preset_private_categories)

    for main_category, subcategories in combined_categories.iteritems():
        if type(subcategories) is list:

            subcategories_ = subcategories[:]
            subcategories_.append(main_category)

            for subcategory in subcategories_:

                if not QueryMachine.category_exists(subcategory):

                    QueryMachine.createCategory(
                        title=subcategory.strip().capitalize(),
                        description="",
                        filename="",
                        eventcounter=0
                    )
    return "ok"