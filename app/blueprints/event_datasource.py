# Blueprint for handling events

from flask import request, Blueprint
from flask.ext.login import login_required,current_user
from app.models.querymachine import QueryMachine
from app.models.eventunit import EventUnit
from app.models.event import Event
from app.models.layer import Layer
from app.models.playlist import Playlist
from app.models.user import Collaboration
import json
from werkzeug import secure_filename
from app.models.querymachine import DBSessionMaker
import hashlib

# Amazon Simple Storage Service
from boto.s3.connection import S3Connection
from config import AWSAccessKeyId,AWSSecretKey,AWS_S3_Bucket
from boto.s3.key import Key

event_datasource = Blueprint('event_datasource', __name__, template_folder='templates')

@event_datasource.route('/get/events/single/json')
def get_single_event_using_query_machine():
    """
    Find a single event using event id. The event is returned as JSON string.
    :rtype: str
    :return: JSON string of the found event object
    """

    id = request.args.get('ID', 1)
    result = QueryMachine.get_single_event_using_query_machine(id)
    return json.dumps(result.serializeJSON) if result else json.dumps("{}")

@event_datasource.route('/get/events/json')
def getEventsUsingQueryMachine():
    """
    Find events with defined search options using GET method
    :returns: JSON of found events
    :rtype: str
    """

    # Never trust the input from users, parse integers from all kinds of input with defaults integers
    how_many = request.args.get('howmany', '5')
    min_significance = request.args.get('minsignificance', '3')
    max_significance = request.args.get('maxsignificance', '10')
    start_date = request.args.get('startdate', '0')
    end_date = request.args.get('enddate', '1')
    search_text = request.args.get('searchtext', '')

    categories = request.args.get('categories', '').split(",")
    categories = filter(lambda x: x.isdigit(), categories)

    # Validate numerical input. Force the values inside defined ranges
    how_many = min(max(3 if not how_many.isdigit() else int(how_many), 1), 50)
    min_significance = min(max(3 if not min_significance.isdigit() else int(min_significance), 1), 10)
    max_significance = min(max(max(10 if not max_significance.isdigit() else int(max_significance), 1), max_significance), 10)

    results = QueryMachine.queryEventsBetween(how_many, min_significance, max_significance, start_date, end_date, search_text, categories)
    return json.dumps(map(lambda x: x.serializeJSON if x and type(x) is Event else "{}", results))

@event_datasource.route('/get/units/json', methods=['GET','POST'])
def getUnitInformationInJSON():
    """
    Fetch unit data using JSON-format. Supports a single retrieval and multiple at the same time. However,
    do not trust that the units are always available.
    :returns: json dump of final data
    """

    selected_units = []
    if request.method == "GET":
        selected_units = request.args.get('selected_units','[]')
    if request.method == "POST":
        selected_units = request.form['selected_units']

    serialize_unit_if_possible = lambda x: x.serializeJSON if x and type(x) is EventUnit else "{}"

    if selected_units.isdigit():
        final_data = serialize_unit_if_possible(QueryMachine.singleUnitData(int(selected_units)))
    else:
        selected_units = json.loads(selected_units)
        final_data = map(lambda x: serialize_unit_if_possible(QueryMachine.singleUnitData(x)), selected_units)

    return json.dumps(final_data)

@event_datasource.route('/search/events/json')
def searchEvents():
    """
    Search events using a keyword. Return only actual found Event objects
    :returns: JSON array of found events
    """

    serialize_event_if_possible = lambda x: x.serializeJSON if x and type(x) is Event else "{}"

    search_text = request.args.get('searchText', '')
    if len(search_text) > 2:
        return json.dumps(map(lambda x: serialize_event_if_possible(x), QueryMachine.searchEvents(search_text)))
    else:
        return json.dumps([])

@event_datasource.route('/search/units/json')
def searchUnits():
    """
    Search units using a searchtext keyword
    :returns: JSON array of found units
    """
    search_text = request.args.get('searchText', '')
    if len(search_text) > 2:
        search_results = QueryMachine.searchUnits(search_text)
        return json.dumps(map(lambda x: x.serializeJSON if x else "{}", search_results))
    else:
        return json.dumps([])

@event_datasource.route('/set/event', methods=['GET','POST'])
@login_required
def setEvent():
    if request.method == 'POST':
        QueryMachine.createEvent(
            request.form['selcats'],
            request.form['selected_preset_topics'],
            author=current_user._get_current_object().id,
            start=request.form['start'],
            end=request.form['stop'],
            title=request.form['title'],
            description=request.form['desc'],
            wikilink=request.form['wiki'],
            significance=request.form['sig'],
            data=request.form['data'],
            filename=request.form['filename'],
            incertitude=request.form['incertitude'],
            incertitude_type=request.form['incertitude_type'],
            visibility_type=request.form['visibility_type']
        )

        return "1"
    return ""

@event_datasource.route('/update/event', methods=['GET', 'POST'])
@login_required
def updateEvent():

    if request.method == 'POST':

        author = current_user._get_current_object().id
        uid = request.form['id']

        # Does the user have correct rights to edit? This means either of
        # being the original author or having privilege of collaboration.
        if QueryMachine.check_user_privilege(author, uid, 0):


            QueryMachine.updateEvent(
                request.form['selcats'],
                request.form['selected_preset_topics'],
                start=request.form['start'],
                end=request.form['stop'],
                title=request.form['title'],
                description=request.form['desc'],
                wikilink=request.form['wiki'],
                significance=request.form['sig'],
                data=request.form['data'],
                id=uid,
                filename=request.form['filename'],
                incertitude=request.form['incertitude'],
                incertitude_type=request.form['incertitude_type'],
                visibility_type=request.form['visibility_type']
            )

            return "H"

    return ""

@event_datasource.route('/delete/event', methods=['GET', 'POST'])
@login_required
def deleteEvent():
    if request.method == 'POST':
        author = current_user._get_current_object().id
        uid = request.form['id']
        with DBSessionMaker() as conn:
            event = Event.query.filter_by(id=uid).first()

            # Check if the user has correct rights to edit?
            if event is not None and event.author == author:
                conn.delete(event)
                conn.commit()

        return "ok"
    return ""

@event_datasource.route('/set/unit/image', methods=['GET','POST'])
@login_required
def uploadUnitImage():
    """
    Upload unit images to the Amazon S3 with a filename, which is a hash
    that is calculated from the image content. This can potentially remove
    duplicates of identical images from users.
    """

    # Create a set of allowed extensions.
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

    def allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    if request.method == 'POST':
        file = request.files['unitMovementImage_'+request.args.get('dir')]

        if file and allowed_file(file.filename):

            filename = secure_filename(file.filename)
            fileExtension = filename.rsplit('.', 1)[1]

            # Open a connection to Amazon S3
            conn = S3Connection(AWSAccessKeyId, AWSSecretKey)

            # Open the existing bucket
            bucket = conn.get_bucket(AWS_S3_Bucket)

            # Initialize a key for a single file
            k = Key(bucket)

            # Read the file contents and calculate the corresponding hash
            image_data = file.read()
            image_hash = hashlib.sha1(image_data).hexdigest()

            # Set metadata
            k.set_metadata("uploader_id", current_user._get_current_object().id)

            # Set hash filename with the given file extension. The filename starts
            # with user id, has a random filename part and has the original picture
            # file format extension. This allows user-specific searches with common prefix.
            k.key = '{2}_{0}.{1}'.format(image_hash, fileExtension,current_user._get_current_object().id)

            print "Uploaded image filename:",k.key

            k.set_contents_from_string(image_data)

            return "http://{1}.s3.amazonaws.com/{0}".format(k.key, AWS_S3_Bucket)

    return 'Upload error'

@event_datasource.route('/set/unit', methods=['GET', 'POST'])
@login_required
def setUnit():
    if request.method == "POST":
        with DBSessionMaker() as conn:
            unit = EventUnit(
                author=current_user._get_current_object().id,
                appears=request.form['start'],
                disappears=request.form['stop'],
                title=request.form['title'],
                location=request.form['location'],
                description=request.form['desc'],
                wikilink=request.form['wikilink'],
                filename=request.form['filename'],
                imageData=request.form['imageData']
            )
            conn.add(unit)
            conn.commit()
        return "ok"
    return ""

@event_datasource.route('/update/unit', methods=['GET', 'POST'])
@login_required
def updateUnit():
    if request.method == "POST":
        author = current_user._get_current_object().id
        uid = request.form['id']

        with DBSessionMaker() as conn:

            # Does the user have correct rights to edit? This means either of
            # being the original author or having privilege of collaboration.
            if QueryMachine.check_user_privilege(author, uid, 1):
                start = request.form['start']
                stop = request.form['stop']

                title, desc, location, wikilink, filename, imageData = map(lambda x: request.form[x], ('title', 'desc', 'location', 'wikilink', 'filename', 'imageData'))

                conn.query(EventUnit).filter_by(id=uid).update({
                    "appears": start,
                    "disappears": stop,
                    "title": title,
                    "location": location,
                    "description": desc,
                    "wikilink": wikilink,
                    "filename": filename,
                    "imageData": imageData
                    }, synchronize_session=False)

                conn.commit()

        return "ok"
    return ""

@event_datasource.route('/delete/unit', methods=['GET','POST'])
@login_required
def deleteUnit():
    if request.method == "POST":
        author = current_user._get_current_object().id
        uid = request.form['id']
        with DBSessionMaker() as conn:
            unit = EventUnit.query.filter_by(id=uid).first()

            # Only authors have the privilege to delete content
            if unit is not None and unit.author == author:
                conn.delete(unit)
                conn.commit()
        return "ok"
    return ""

@event_datasource.route('/set/collaborators', methods=['GET', 'POST'])
@login_required
def set_collaborators():
    if request.method == "POST":

        author = current_user._get_current_object().id

        username = request.form['collaborator']
        content_id = request.form['content_id']
        content_type = request.form['content_type']

        content_id = int(content_id) if content_id.isdigit() else None
        content_type = int(content_type) if content_type.isdigit() else None

        user_model = QueryMachine.find_user_by_username(username)

        if user_model:

            # Check that the person assigning the collaborators is the
            # original author of the content.
            if not QueryMachine.check_user_privilege(author, content_id, content_type, True):
                return False

            collaboration = Collaboration(user_model.id, content_id, content_type)

            with DBSessionMaker() as conn:
                conn.add(collaboration)
                conn.commit()

            return json.dumps({"username": user_model.username, "id": user_model.id})

    return ""

@event_datasource.route('/get/collaborators', methods=['GET', 'POST'])
@login_required
def get_collaborators():
    if request.method == "POST":
        content_id = request.form['content_id']
        content_type = request.form['content_type']

        content_id = int(content_id) if content_id.isdigit() else None
        content_type = int(content_type) if content_type.isdigit() else None

        return QueryMachine.list_collaborators_for_user_interface(content_id, content_type)
    return "[]"

@event_datasource.route('/remove/collaborators', methods=['GET','POST'])
@login_required
def remove_collaborators():
    # Content types: 0=Event,1=Unit,2=Layer,3=Playlist
    content_types = (Event, EventUnit, Layer, Playlist)
    author = current_user._get_current_object().id

    with DBSessionMaker() as conn:

        user_id = request.form['user_id']
        content_id = request.form['content_id']
        content_type = request.form['content_type']

        user_id = int(user_id) if user_id.isdigit() else None
        content_id = int(content_id) if content_id.isdigit() else None
        content_type = int(content_type) if content_type.isdigit() else None

        find_collaboration = conn.query(Collaboration).filter_by(
            user_id=user_id,
            resource_id=content_id,
            resource_type=content_type
        ).first()

        # Check that the collaboration exists in the first place
        if find_collaboration is not None:

            # Retrieve the content, because we want to be sure that the person removing
            # the collaborator is the original author
            content_record = content_types[content_type].query.filter_by(id=content_id).first()

            # Only authors have the privilege to remove collaborators
            if content_record is not None and content_record.author == author:
                conn.delete(find_collaboration)
                conn.commit()
                return "ok"

        return ""

@event_datasource.route('/get/your/content', methods=['GET', 'POST'])
@login_required
def your_content():
    author = current_user._get_current_object().id
    content_types = (Event, EventUnit, Layer, Playlist)

    if request.method == "POST":

        # Container for final results, which are transformed into JSON
        pile_of_json = []

        for i_, content_type in enumerate(content_types):

            # Find content where the user is the original author
            pile_of_json.extend(
                map(lambda x: x.serializeJSON,
                    content_type.query.filter_by(author=author).all()
                )
            )

            # Find content where the user has collaboration rights
            collaboration_content = Collaboration.query.filter_by(
                user_id=author,
                resource_type=i_
            ).all()

            list_of_collaboration_ids = map(lambda x: x.resource_id, collaboration_content)

            pile_of_json.extend(
                map(lambda x: content_type.query.filter_by(id=x).first().serializeJSON,
                    list_of_collaboration_ids
                )
            )

        return json.dumps(pile_of_json) if pile_of_json else json.dumps("{}")

    return "{}"