from flask import Blueprint, request
from flask.ext.login import login_required,current_user
from app.models.layer import Layer
import json
from app.models.querymachine import DBSessionMaker, QueryMachine

layer_datasource = Blueprint('layer_datasource', __name__, template_folder='templates')

@layer_datasource.route('/set/layer', methods=['GET', 'POST'])
@login_required
def setLayer():
    if request.method == 'POST':
        with DBSessionMaker() as conn:

            layer = Layer(
                author=current_user._get_current_object().id,
                start=request.form['start'],
                stop=request.form['stop'],
                description=request.form['description'],
                title=request.form['title'],
                data=request.form['data']
            )
            conn.add(layer)
            conn.commit()
        return "OK"
    return "ERR"

@layer_datasource.route('/update/layer', methods=['GET', 'POST'])
@login_required
def updateLayer():
    if request.method == 'POST':
        with DBSessionMaker() as conn:
            author = current_user._get_current_object().id
            uid = request.form['id']

            # Does the user have correct rights to edit? This means either of
            # being the original author or having privilege of collaboration.
            if QueryMachine.check_user_privilege(author, uid, 2):

                start = request.form['start'],
                stop = request.form['stop'],
                description = request.form['description']
                title = request.form['title']
                data = request.form['data']
                conn.query(Layer).filter_by(id=uid).update({
                    "start": start,
                    "stop": stop,
                    "title": title,
                    "description": description,
                    "data": data
                }, synchronize_session=False)
                conn.commit()

        return "OK"
    return "ERR"

@layer_datasource.route('/delete/layer', methods=['GET', 'POST'])
@login_required
def deleteLayer():
    if request.method == 'POST':
        with DBSessionMaker() as conn:
            lid = request.form['lid']
            author = current_user._get_current_object().id
            layer = Layer.query.filter_by(id=lid).first()
            if layer is not None and layer.author == author:
                conn.delete(layer)
                conn.commit()
        return "OK"
    return "ERR"

@layer_datasource.route('/search/layer', methods=['GET', 'POST'])
def layer_search():
    search_text = request.args.get('searchtext', '')
    if len(search_text) > 2:
        with DBSessionMaker() as conn:
            search_result = conn.query(Layer).filter(Layer.title.like("%{0}%".format(search_text))).limit(10).all()
        return json.dumps(map(lambda x: x.serializeJSON, search_result))
    return "{}"