# Blueprint for testing purposes

from flask import Blueprint, render_template, g, session
from flask.ext.login import current_user,login_required
from app.forms.login import LoginForm
from app.forms.register import RegisterForm
from app.models.user import User
from app.models.category import Category
from app import lm
from config import preset_public_categories,preset_private_categories

main_page = Blueprint('main_views', __name__, template_folder='templates')

@main_page.before_request
def before_request():
    g.user = current_user

@lm.user_loader
def load_user(id):
    return User.query.get(int(id))



# Render the main view of Wikistoria
@main_page.route('/')
def index():
    form = LoginForm()
    regform = RegisterForm()
    return render_template(
        'index.html',
        form=form,
        regform=regform,
        topic_public_selectors=Category.presetTopicHTMLCheckboxes(preset_public_categories,"event_editor"),
        topic_private_selectors=Category.presetTopicHTMLCheckboxes(preset_private_categories,"event_editor")
    )

@main_page.route('/admin')
@login_required
def admin():
    return "admin with id {0}".format(current_user._get_current_object().id)