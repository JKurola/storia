# Blueprint for managing everything related to playlists

from flask import Blueprint, request, jsonify, Response
from flask.ext.login import login_required,current_user
from jinja2 import Environment
from app.models.querymachine import QueryMachine
from app.models.playlist import Playlist
from app.database import db_session
from sqlalchemy import func
import json
from app.models.querymachine import DBSessionMaker

playlist_datasource = Blueprint('playlist_datasource', __name__, template_folder='templates')

@playlist_datasource.route('/get/single/json', methods=['GET', 'POST'])
def playlist_info():
    """
    Get a single playlist by a given ID
    :return: playlist data in JSON format
    """
    selected_playlist = request.args.get('selected_playlist',0)
    final_data = json.loads('[]')
    if isinstance(selected_playlist,str) and selected_playlist.isdigit():
        final_data = QueryMachine.playlist_getSingleData(int(selected_playlist)).serializeJSON
    return json.dumps(final_data)

@playlist_datasource.route('/search/json', methods=['GET', 'POST'])
def playlist_search():
    """
    Find playlist with a given search text. The search text is given as
    GET parameter 'searchText'. It's value has to have at least three
    characters.
    :rtype: str
    :return: JSON string that contains the found playlists
    """
    search_text = request.args.get('searchText', '')
    if len(search_text) > 2:
        search_results = QueryMachine.playlist_search(search_text)
        return json.dumps(map(lambda x: x.serializeJSON, search_results))
    else:
        return json.dumps([])

@playlist_datasource.route('/update/json', methods=['GET', 'POST'])
@login_required
def playlist_update():
    """
    Update a playlist. The parameters are given as POST parameters. The parameters are:
    * 'id' : integer; the index number of edited playlist
    * 'title' : str; Playlist name
    * 'description' : str; Textual description of the playlist
    * 'filename' : str; URL of the picture of the playlist (icon)
    * 'data' : str; JSON string of the playlist contents (data)
    :rtype: str
    """

    author = current_user._get_current_object().id
    uid = request.form['id']

    # Does the user have correct rights to edit? This means either of
    # being the original author or having privilege of collaboration.
    if QueryMachine.check_user_privilege(author, uid, 3):

        QueryMachine.updatePlaylist(
            id=request.form['id'],
            author=current_user._get_current_object().id,
            title=request.form['title'],
            description=request.form['description'],
            edited=func.now(),
            filename=request.form['filename'],
            data=request.form['data']
        )

    return "H"

@playlist_datasource.route('/create/json', methods=['GET', 'POST'])
@login_required
def playlist_create():
    """
    Create a playlist. The parameters are given as POST parameters. The parameters are:
    * 'title' : str; Playlist name
    * 'description' : str; Textual description of the playlist
    * 'filename' : str; URL of the picture of the playlist (icon)
    * 'data' : str; JSON string of the playlist contents (data)
    :rtype: str
    """
    QueryMachine.createPlaylist(
        author=current_user._get_current_object().id,
        title=request.form['title'],
        description=request.form['description'],
        edited=func.now(),
        filename=request.form['filename'],
        data=request.form['data']
    )
    return "H"

@playlist_datasource.route('/delete/json', methods=['GET', 'POST'])
@login_required
def playlist_delete():
    """
    Delete a playlist with a given playlist index. The index is given as a parameter
    for POST method. The parameter is 'id'.
    :rtype: str
    """
    author = current_user._get_current_object().id
    uid = request.form['id']
    with DBSessionMaker() as conn:
        playlist = Playlist.query.filter_by(id=uid).first()
        if playlist is not None and playlist.author == author:
            conn.delete(playlist)
            conn.commit()
    return "ok"

@playlist_datasource.route('/export/json/<int:id_>')
def playlist_export_json(id_):
    """
    Export a playlist as JSON with given id. The index is given as a parameter
    for GET method. The parameter is 'id'.playlist. The resulting JSON is returned
    as a string.
    :rtype: str
    :return: JSON string of the exported playlist
    """
    playlist = QueryMachine.playlist_getSingleData(id_)
    return jsonify(playlist.serializeJSON) if playlist else jsonify(error="Not found")

@playlist_datasource.route('/export/xml/<int:id_>')
def playlist_export_xml(id_):
    """
    Export a playlist as XML with given id. The index is given as a parameter
    for GET method. The parameter is 'id'.playlist. The resulting XML is returned
    as a string.
    :rtype: str
    :return: XML string of the exported playlist
    """
    playlist = QueryMachine.playlist_getSingleData(id_)

    if playlist:
        sel_events = json.loads(playlist.data)
        xml_template = """<playlist>
<description>{{ playlist.description }}</description>
<created>{{ playlist.created }}</created>
<edited>{{ playlist.edited }}</edited>
<author>{{ playlist.author }}</author>
<filename>{{ playlist.filename }}</filename>
<id>{{ playlist.id }}</id>
<events>
{% for selEv in selEvents%}
<event id="{{ selEv }}" />
{% endfor %}
</events>
</playlist>"""
        template_renderer = Environment().from_string(xml_template)
        data = template_renderer.render(playlist=playlist, selEvents=sel_events["IDs"])
    else:
        data = "<playlist><error>Not found</error></playlist>"
    return Response(data, mimetype='text/xml')