# Blueprint for testing purposes

from flask import Blueprint,session,redirect,url_for,flash,request
from flask.ext.login import login_user,logout_user,login_required,current_user
from app.forms.login import LoginForm
from app.forms.register import RegisterForm
from app.models.user import User
from app.database import db_session
from app.models.querymachine import QueryMachine
import hashlib
import json
from app.models.querymachine import DBSessionMaker
from config import UI_MESSAGES_SUCCESSFUL_LOGIN, UI_MESSAGES_UNSUCCESSFUL_LOGIN
from config import UI_MESSAGES_ERROR_LOGIN, UI_MESSAGES_SUCCESSFUL_REGISTER
from config import UI_MESSAGES_NAME_TAKEN_REGISTER, UI_MESSAGES_ERROR_REGISTER

user_page = Blueprint('user_page', __name__, template_folder='templates')

@user_page.route("/user/new/password", methods=['POST'])
def user_new_password():
    email_ = request.form['email']
    username_ = request.form['username']
    return QueryMachine.user_new_password(username_,email_)

@user_page.route("/name_query", methods=['GET','POST'])
def nameQuery():
    id_ = request.form['author']
    user_ = QueryMachine.find_username_by_id(id_)
    return user_.username

@user_page.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        attemptedPasswordHash = hashlib.sha512(form.password.data).hexdigest()
        with DBSessionMaker() as conn:
            user = conn.query(User).filter_by(username = form.username.data).first()

        if user is not None and user.password == attemptedPasswordHash:
            session["uid"] = user.id
            login_user(user)
            flash(UI_MESSAGES_SUCCESSFUL_LOGIN.format(user.username), "")
        else:
            flash(UI_MESSAGES_UNSUCCESSFUL_LOGIN, "error")

    else:
        flash(UI_MESSAGES_ERROR_LOGIN, "error")

    return redirect(url_for("main_views.index"))

@user_page.route("/user/update/json", methods=['GET','POST'])
@login_required
def user_update_json():
    QueryMachine.updateUser(
        id=current_user.id,
        email=request.form['email'].strip(),
        realname=request.form['realname'].strip(),
        description=request.form['description']
    )
    return ""

@user_page.route("/user/info/json", methods=['GET','POST'])
@login_required
def user_info_json():
    return json.dumps(current_user.serializeJSON)

@user_page.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out","")
    return redirect(url_for('main_views.index'))

@user_page.route("/register", methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        password = form.password.data
        username = form.username.data
        email = form.email.data

        if not QueryMachine.find_user_by_username(username):

            user = User(username, email, password)

            db_session.add(user)
            db_session.commit()

            flash(UI_MESSAGES_SUCCESSFUL_REGISTER.format(username), "success")
        else:
            flash(UI_MESSAGES_NAME_TAKEN_REGISTER.format(username), "error")

    else:
        flash(UI_MESSAGES_ERROR_REGISTER, "error")

    return redirect(url_for("main_views.index"))