
# Copy & pasted from http://flask.pocoo.org/docs/patterns/sqlalchemy/

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import QueuePool
from config import SQLALCHEMY_DATABASE_URI

engine = create_engine(SQLALCHEMY_DATABASE_URI, convert_unicode=True, pool_recycle=10, pool_size=5, poolclass=QueuePool)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    from app.models.event import Event
    from app.models.user import User
    from app.models.layer import Layer
    from app.models.eventunit import EventUnit
    from app.models.playlist import Playlist
    from app.models.category import Category
    Base.metadata.create_all(bind=engine)