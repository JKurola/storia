from flask.ext.wtf import Form, TextField, PasswordField, Required, Email, EqualTo

class LoginForm(Form):
	username = TextField("username", [Required()])
	password = PasswordField("password", [Required()])