from flask.ext.wtf import Form, TextField, PasswordField, Required, Email, EqualTo

class RegisterForm(Form):
	username = TextField("username", [Required()])
	email = TextField("email", [Required(), Email()])
	password = PasswordField("password", [Required()])
	confirm = PasswordField("Repeat Password", [Required(), EqualTo("password", message="Passwords do not match")])