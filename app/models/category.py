from app.database import Base
from sqlalchemy import Column,Integer,String,Text
from config import preset_private_categories,preset_public_categories

class Category(Base):
    __tablename__ = 'categories'
    id = Column(Integer, primary_key=True)
    title = Column(String(500))
    description = Column(Text)

    # Category icon image
    filename = Column(Text)

    # Counter for number of events flagging the category, more efficient than
    # querying and summing the number from many-to-many table every time
    eventcounter = Column(Integer)

    # Render the preset topics that are defined in advance
    @staticmethod
    def presetTopicHTMLCheckboxes(data,selector_prefix=""):
        """
        :type data: dict
        :type selector_prefix: str
        """

        finalHTML = ""

        selectorTemplate = '<input ' \
                           'class="preset_topic_selector_class_{selector_prefix}" ' \
                           'type="checkbox" ' \
                           'id="preset_topic_selector_{selector_prefix}__{topic_name_no_underscores}" ' \
                           '/> ' \
                           '<span style="font-weight:{font_weight}">' \
                           '{topic_name}' \
                           '</span>'

        renderSelectorTemplate = lambda topic_name, font_weight: selectorTemplate.format(
            topic_name=topic_name,
            topic_name_no_underscores=topic_name.replace(" ","_"),
            selector_prefix=selector_prefix,
            font_weight=font_weight
        )

        # The preset topics are contained in a dictionary by definition
        if type(data) is not dict:
            return ""

        for main_category, categories in data.iteritems():

            # Render the main category itself
            if type(main_category) is str:
                finalHTML += renderSelectorTemplate(main_category.capitalize(), "bold")
                finalHTML += "<br/>"

            # The main category has a list of subcategories
            if type(categories) is list:
                for subcategory in categories:
                    finalHTML += renderSelectorTemplate(subcategory.capitalize(), "auto")
                    finalHTML += "<br/>"
                finalHTML += "<hr />"

            # The main category has only a single subcategory (as a string)
            if type(categories) is str:
                finalHTML += renderSelectorTemplate(categories.capitalize(), "auto")

        return finalHTML

    @staticmethod
    def include_main_categories(list_of_categories):
        """
        The method forces a list of categories to include their respective
        main categories.
        :type list_of_categories: list
        :return list
        """
        # Find the main categories and append them in result
        result = list_of_categories + map(lambda x: Category.mainCategory(x), list_of_categories)
        # Remove duplicates and return
        return list(set(result))

    # Hard-coded main categories for Wikistoria implementation
    @staticmethod
    def mainCategory(title_):
        clean_title = title_.lower().strip().replace("_", " ")

        for main_category in preset_public_categories:
            if clean_title in preset_public_categories[main_category]:
                return main_category

        for main_category in preset_private_categories:
            if clean_title in preset_private_categories[main_category]:
                return main_category

        return ''

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        return "<Model Category> Title={0}".format(self.title)

    @property
    def serializeJSON(self):
        """Return object data in easily serializable format"""

        return {
            'id': self.id,
            'title': self.title,
            'eventcounter': self.eventcounter,
            'filename': self.filename,
            'description': self.description,
            'modeltype': 'category',
            'main_category': Category.mainCategory(self.title)
        }