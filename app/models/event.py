from app.database import Base
from sqlalchemy import Column,Integer,String,Text,DateTime,func,ForeignKey,Table,BigInteger
from sqlalchemy.orm import relationship
from app.models.category import Category
import json

event_m2m_category = Table('event_category_m2m', Base.metadata,
    Column('id_event', Integer, ForeignKey('events.id')),
    Column('id_category', Integer, ForeignKey('categories.id'))
)

class Event(Base):
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True)
    author = Column(Integer, ForeignKey('users.id'))
    start = Column(BigInteger)
    end = Column(BigInteger)
    title = Column(String(500))
    description = Column(Text)
    significance = Column(Integer)
    wikilink = Column(Text)
    filename = Column(Text)
    visibility_type = Column(Integer, default=0)
    incertitude = Column(Integer)
    incertitude_type = Column(Integer)

    # Unidirectional many to many mapping between events and categories. Categories don't need to know
    # about the flagged events, so no need to use backref in relationship mapping
    categories_relationship = relationship("Category", secondary=event_m2m_category, lazy="joined")

    # Code the list of actions/changes in event in JSON-format. The data consists
    # of movement dates for an eventunit id. Therefore, we can just load and render events between two dates,
    # and read all the unit placements inside events, and then interpolate correct locations using datetimes.
    data = Column(Text)

    def _get_categories(self):
        return [x.serializeJSON for x in self.categories_relationship]

    def __repr__(self):
        return "<Model EVENT> Title={0}".format(self.title)

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    @property
    def serializeJSON(self):
        """Return object data in easily serializable format"""

        # JSON crashes if data is empty, therefore ensure using elementary
        # ternary operator that JSON loads always at least an empty array
        validateData = lambda data: data if data else "[]"

        return {
            'start': self.start,
            'end': self.end,
            'content': self.title,
            'data': json.loads(validateData(self.data)),
            'description': self.description,
            'significance': self.significance,
            'wikilink': self.wikilink,
            'author': self.author,
            'id': self.id,
            'filename': self.filename,
            'modeltype': 'event',
            'title': self.title,
            'categories': self._get_categories(),
            'visibility_type': self.visibility_type,
            'incertitude' : self.incertitude,
            'incertitude_type' : self.incertitude_type
        }
