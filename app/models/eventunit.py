from app.database import Base
from sqlalchemy import Column, Integer, String, Text, DateTime, func, ForeignKey, BigInteger
import json

class EventUnit(Base):
    """
    Unit icons for events. User can upload event icon with metainformation. Metainformation
    contains for example time period when the icon was available. For example, an ancient
    Roman military unit and modern infantry military unit were available at different times.
    """
    __tablename__ = 'eventunits'
    id = Column(Integer, primary_key=True)
    author = Column(Integer, ForeignKey('users.id'))
    appears = Column(BigInteger)
    disappears = Column(BigInteger)
    title = Column(String(500))
    location = Column(String(500))
    description = Column(Text)
    wikilink = Column(Text)
    filename = Column(Text)
    imageData = Column(Text)

    def __init__(self,**kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        return "<Model EVENTUNIT> Title={title}".format(title=self.title)

    @property
    def serializeJSON(self):
        """Return object data in easily serializable format"""

        return {
           'id': self.id,
           'author': self.author,
           'appears': self.appears,
           'disappears': self.disappears,
           'title': self.title,
           'location': self.location,
           'description': self.description,
           'wikilink': self.wikilink,
           'filename': self.filename,
           'modeltype': 'unit',
           'imageData': json.loads(self.imageData) if self.imageData != "" else {}
        }