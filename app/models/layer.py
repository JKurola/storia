from app.database import Base
from sqlalchemy import Column, Integer, String, Text, DateTime, func, ForeignKey, BigInteger
import json

class Layer(Base):
    __tablename__ = 'layers'
    id = Column(Integer, primary_key=True)
    author = Column(Integer, ForeignKey('users.id'))
    start = Column(BigInteger)
    stop = Column(BigInteger)
    title = Column(String(500))
    description = Column(Text)

    # Code the list of actions/changes in event in JSON-format
    data = Column(Text)

    def __repr__(self):
        return "<Model LAYER> {title}".format(title=self.title)
    
    def dataToJSON(self):
        return json.loads(self.data)

    def JSONToData(self,data):
        self.data = json.dumps(data)
    
    @property
    def serializeJSON(self):
        """Return object data in easily serializable format"""

        # JSON crashes if data is empty, therefore ensure that JSON loads always at least an empty array
        validateData = lambda data: data if data else "[]"

        return {
           'id': self.id,
           'author': self.author,
           'title': self.title,
           'data': json.loads(validateData(self.data)),
           'start': self.start,
           'stop': self.stop,
           'description': self.description,
           'modeltype' : 'layer'
        }