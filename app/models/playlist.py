from app.database import Base
from sqlalchemy import Column,Integer,String,Text,DateTime,func,ForeignKey,Binary
import json

def prepareOldDates(value):
    return value.isoformat(" ").split(".")[0]

class Playlist(Base):
    __tablename__ = 'playlists'
    id = Column(Integer, primary_key=True)
    author = Column(Integer, ForeignKey('users.id'))
    title = Column(String(500))
    description = Column(Text)
    created = Column(DateTime, default=func.now())
    edited = Column(DateTime, default=func.now())
    private = Column(Binary, default=False)

    # Playlist icon image
    filename = Column(Text)

    # JSON listing of Event IDs
    data = Column(Text)

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        return "<Model Playlist> Significance={0}".format(self.significance)

    @property
    def serializeJSON(self):
        """Return object data in easily serializable format"""

        # JSON crashes if data is empty, therefore ensure using elementary
        # ternary operator that JSON loads always at least an empty array
        validateData = lambda data: data if data else "[]"

        return {
            'id': self.id,
            'author': self.author,
            'title': self.title,
            'data': json.loads(validateData(self.data)),
            'created': prepareOldDates(self.created),
            'edited': prepareOldDates(self.edited),
            'filename': self.filename,
            'description': self.description,
            'modeltype':'playlist'
        }
