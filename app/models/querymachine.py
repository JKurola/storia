from app.database import db_session
from app.models.event import Event
from app.models.eventunit import EventUnit
from app.models.playlist import Playlist
from app.models.category import Category
from app.models.layer import Layer
from app.models.user import User, Collaboration
from sqlalchemy import and_, or_
import json
import random
import string

# Send email using Mailgun HTTP API with HTTP POST-requests
import requests
from config import MAILGUN_Api_Key, MAILGUN_HTTP_API_URL, DTF_UserInterface, DTF_MySQL, NEWPASSWORD_EMAIL_SUBJECT
import datetime

class DBSessionMaker:
    conn = None

    def __init__(self):
        self.conn = None

    def __enter__(self):
        self.conn = db_session()
        return self.conn

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.close()

class QueryMachine:

    @staticmethod
    def queryEventsBetween(
            howmany=4,
            minsignificance=1,
            maxsignificance=10,
            startDate=-1000000,
            endDate=10000000000,
            searchText='',
            categories=()
    ):
        """
        Query events from database. The events are sorted by their importance, and
        a specified number of events is selected.
        :param startDate; Integer
        :param endDate; Integer
        :param howmany: Integer
        :param maxsignificance: Integer
        :param minsignificance: Integer
        :param searchText: String
        :return: List of found events
        """
        with DBSessionMaker() as conn:

            if not searchText and not categories:
                results = conn.query(Event).filter(and_(
                        Event.significance >= minsignificance,
                        Event.significance <= maxsignificance,
                        Event.start >= startDate,
                        Event.end <= endDate,
                        Event.visibility_type < 3
                        )).order_by(Event.significance.desc()).limit(howmany).all()
            elif searchText and not categories:
                results = conn.query(Event).filter(and_(
                        Event.significance >= minsignificance,
                        Event.significance <= maxsignificance,
                        Event.start >= startDate,
                        Event.end <= endDate,
                        Event.visibility_type < 3,
                        or_(Event.title.like("%{0}%".format(searchText)),Event.description.like("%{0}%".format(searchText)))
                        )).order_by(Event.significance.desc()).limit(howmany).all()
            else:
                results = conn.query(Event).filter(and_(
                        Event.significance >= minsignificance,
                        Event.significance <= maxsignificance,
                        Event.start >= startDate,
                        Event.end <= endDate,
                        Event.visibility_type < 3,
                        Event.categories_relationship.any(Category.id.in_(categories)),
                        or_(Event.title.like("%{0}%".format(searchText)),Event.description.like("%{0}%".format(searchText)))
                        )).order_by(Event.significance.desc()).limit(howmany).all()

        return results

    @staticmethod
    def transformIntoSQLDateTime(raw_date, error_default="01.01.1000 10:00:00"):
        """
        Parse datetime strings into ISO 8601 format. This is implemented using the isoformat-method
        in Python datetime-objects.
        """

        # Try parsing the given datetime in user interface datetime format.
        try:
            parsed_result = datetime.datetime.strptime(raw_date, DTF_UserInterface).isoformat(' ')
        except ValueError:
            # Failed to parse the given datetime string. Return a default datetime string.
            parsed_result = ""

        if not parsed_result:
            # Check if the given datetime is already in MySQL datetime format.
            try:
                parsed_result = datetime.datetime.strptime(raw_date, DTF_MySQL).isoformat(' ')
            except ValueError:
                parsed_result = ""

        return parsed_result if parsed_result else error_default

    @staticmethod
    def user_new_password(username_,email_):
        with DBSessionMaker() as conn:
            user = conn.query(User).filter(and_(User.username == username_, User.email == email_)).limit(1).first()

            if user:
                new_password = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(7))
                user.updatePassword(new_password)
                conn.commit()

                # Define the payload data for MailGun HTTP API
                payload = {
                    'from': "Wikistoria password system <password@wikistoria.net>",
                    'to': "{0} <{1}>".format(username_,email_),
                    'subject': NEWPASSWORD_EMAIL_SUBJECT,
                    'text': "You have requested a new password for user {username}. The new password is {newPass}".format(username=username_, newPass=new_password)
                }

                # Create a new HTTP request
                s = requests.Session()

                # Supply login credentials for authorization
                s.auth = ('api', MAILGUN_Api_Key)

                # Contact the MailGun HTTP API using HTTP request with POST-method. This
                # sends the actual email.
                s.post(MAILGUN_HTTP_API_URL, data=payload)

        return "1" if user else "0"

    @staticmethod
    def get_single_event_using_query_machine(ID):
        """
        Query a single event from database
        :param ID: Event ID
        :return: JSON data mapping of a single event
        """
        with DBSessionMaker() as conn:
            return conn.query(Event).filter(Event.id == ID).limit(1).first()

    @staticmethod
    def singleUnitData(id_):
        with DBSessionMaker() as conn:
            return conn.query(EventUnit).filter(EventUnit.id == id_).limit(1).first()

    @staticmethod
    def searchUnits(searchText):
        with DBSessionMaker() as conn:
            results = conn.query(EventUnit).filter(
                or_(EventUnit.title.like("%{0}%".format(searchText)), EventUnit.description.like("%{0}%".format(searchText)))
            ).limit(5).all()
        return results

    @staticmethod
    def searchEvents(searchText):
        with DBSessionMaker() as conn:
            results = conn.query(Event).filter(
                or_(Event.title.like("%{0}%".format(searchText)), Event.description.like("%{0}%".format(searchText)))
            ).limit(5).all()
        return results

    @staticmethod
    def find_username_by_id(id_):
        with DBSessionMaker() as conn:
            return conn.query(User).filter(User.id == id_).limit(1).first()

    @staticmethod
    def playlist_getSingleData(id_):
        with DBSessionMaker() as conn:
            return conn.query(Playlist).filter(Playlist.id == id_).limit(1).first()

    @staticmethod
    def find_user_by_username(username_):
        with DBSessionMaker() as conn:
            return conn.query(User).filter(User.username == username_).limit(1).first()

    @staticmethod
    def playlist_search(search_text):
        """
        Search public playlists with given search text.
        """
        with DBSessionMaker() as conn:
            results = conn.query(Playlist).filter(
                or_(Playlist.title.like("%{0}%".format(search_text)), Playlist.description.like("%{0}%".format(search_text)))
            ).filter(Playlist.private == False).limit(10).all()
        return results

    @staticmethod
    def extract_separate_topics(data):
        separate_topics = []
        if data and (type(data) is unicode or type(data) is str):
            separate = data.split(";")
            if separate:
                separate_topics = map(lambda x: x.split("__")[1].replace("_"," "), separate)
        return separate_topics

    @staticmethod
    def updateEvent(tagCollection, selected_preset_topics, **kwargs):
        """
        :type tagCollection: str
        :type selected_preset_topics: str
        """
        with DBSessionMaker() as conn:
            conn.query(Event).filter_by(id=kwargs["id"]).update(kwargs, synchronize_session=False)

            event = conn.query(Event).filter_by(id=kwargs["id"]).first()

            if event:

                separate_topics = QueryMachine.extract_separate_topics(selected_preset_topics)
                separate_topics = Category.include_main_categories(separate_topics)

                if separate_topics:
                    event.categories_relationship = []
                    for topic_ in separate_topics:
                        cat_ = Category.query.filter_by(title=topic_.lower().strip()).first()
                        if cat_:
                            event.categories_relationship.append(cat_)

            conn.commit()

        return "1"

    @staticmethod
    def createEvent(tagCollection, selected_preset_topics, **kwargs):
        """
        :type tagCollection: str
        :type selected_preset_topics: str
        """
        with DBSessionMaker() as conn:
            event = Event(**kwargs)

            separate_topics = QueryMachine.extract_separate_topics(selected_preset_topics)
            separate_topics = Category.include_main_categories(separate_topics)
            if separate_topics:
                for topic_ in separate_topics:
                    cat_ = Category.query.filter_by(title=topic_.lower().strip()).first()
                    if cat_:
                        event.categories_relationship.append(cat_)

            conn.add(event)
            conn.commit()

        return event

    @staticmethod
    def list_collaborators_for_user_interface(content_id, content_type):
        """
        List collaborators for a given content

        :param user_id: User ID number
        :param content_type: Content type identifier (0=Event,1=Unit,2=Layer,3=Playlist)
        :type user_id: int
        :type content_type: int
        :rtype: JSON of users
        """

        def pluck_relevant_information(record):
            return {'username': record.username, 'id': record.id}

        with DBSessionMaker() as conn:
            user_ids = conn.query(Collaboration).filter_by(
                resource_id=content_id,
                resource_type=content_type
            ).all()

            user_names = map(lambda x: x.user_id, user_ids)
            user_names = map(lambda x: pluck_relevant_information(User.query.filter_by(id=x).first()), user_names)

            return json.dumps(user_names)

    @staticmethod
    def check_user_privilege(user_id, content_id, content_type, only_author=False):
        """
        Check if an user has a right to edit the given content. The static method returns
        a boolean value that indicates whether the user is given privileges to collaborate
        or not. If the user is the author of the content, then the user has automatically
        the right to edit the content. Collaborators can edit content but only the author
        may delete the content.

        :param user_id: User ID number
        :param content_id: Content ID number
        :param content_type: Content type identifier (0=Event,1=Unit,2=Layer,3=Playlist)
        :type user_id: int
        :type content_id: int
        :type content_type: int
        :rtype: bool
        """
        with DBSessionMaker() as conn:

            if not only_author:
                # Check for collaborative rights
                if conn.query(Collaboration).filter_by(
                    user_id=user_id,
                    resource_id=content_id,
                    resource_type=content_type
                ).first() is not None:
                    return True

            content_type_mapping = {0: Event, 1: EventUnit, 2: Layer, 3: Playlist}

            # Check for authorship of user_id of the given content
            return conn.query(content_type_mapping.get(
                content_type, Event
            )).filter_by(
                id=content_id,
                author=user_id
            ).first() is not None

    @staticmethod
    def createCategory(**kwargs):
        with DBSessionMaker() as conn:
            conn.add(Category(**kwargs))
            conn.commit()
        return "1"
        
    @staticmethod
    def createLayer(**kwargs):
        with DBSessionMaker() as conn:
            conn.add(Layer(**kwargs))
            conn.commit()
        return "1"

    @staticmethod
    def updateUser(**kwargs):
        with DBSessionMaker() as conn:
            conn.query(User).filter_by(id=kwargs["id"]).update(kwargs, synchronize_session=False)
            conn.commit()
        return "1"

    @staticmethod
    def createPlaylist(**kwargs):
        with DBSessionMaker() as conn:
            conn.add(Playlist(**kwargs))
            conn.commit()
        return "1"

    @staticmethod
    def updatePlaylist(**kwargs):
        with DBSessionMaker() as conn:
            conn.query(Playlist).filter_by(id=kwargs["id"]).update(kwargs, synchronize_session=False)
            conn.commit()
        return "1"

    @staticmethod
    def category_search(searchText):
        with DBSessionMaker() as conn:
            return conn.query(Category).filter(
                or_(Category.title.like("%{0}%".format(searchText)),Category.description.like("%{0}%".format(searchText)))
            ).limit(10).all()

    @staticmethod
    def category_exists(category_name):
        with DBSessionMaker() as conn:
            return conn.query(Category).filter_by(title=category_name.lower().strip()).first() is not None