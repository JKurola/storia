from app.database import Base
from sqlalchemy import Column, Integer, String, Text, DateTime, func, ForeignKey, SmallInteger
import hashlib

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    realname = Column(String(128))
    username = Column(String(30))
    email = Column(String(120))
    description = Column(Text)
    registered = Column(DateTime, default=func.now())
    level = Column(Integer, default=1)
    
    # SHA-512 hash
    password = Column(String(128))

    def __repr__(self):
        return "<Model USER> {username}".format(username=self.username)

    def __init__(self, username, email, password):
        """
        Create a new user. The supplied password will be hashed automatically.
        """
        self.username = username
        self.email = email
        self.password = hashlib.sha512(password).hexdigest()

    def updatePassword(self, password):
        self.password = hashlib.sha512(password).hexdigest()
        
    def is_authenticated(self):
        return True
    
    def is_anonymous(self):
        return False

    def is_active(self):
        return True
    
    def get_id(self):
        return unicode(self.id)

    @property
    def serializeJSON(self):
        """Return object data in easily serializable format"""

        return {
           'id':self.id,
           'realname':self.realname,
           'username':self.username,
           'email':self.email,
           'description':self.description,
           'registered':self.registered.isoformat(" ").split(".")[0],
           'modeltype':'user'
        }

class Collaboration(Base):
    """
    Table that contains the collaboration privileges for users. Content author
    may give collaboration rights to different users. This allows the users
    to edit the content together.
    """
    __tablename__ = 'collaborations'
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    resource_id = Column(Integer, nullable=False)

    # 0=Event,1=Unit,2=Layer,3=Playlist
    resource_type = Column(SmallInteger, default=0, nullable=False)
    role = Column(Integer, default=1, nullable=False)

    def __init__(self, user_id, resource_id, resource_type):
        self.resource_id = resource_id
        self.resource_type = resource_type
        self.user_id = user_id
