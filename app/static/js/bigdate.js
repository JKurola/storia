/**
 * Created by Niko Reunanen on 20.7.2014.
 */

// Knockout.js binding for the big date selector
ko.bindingHandlers.connect_bigdate = {
    init: function(element, valueAccessor) {
        var value = ko.unwrap(valueAccessor());
        var valueUnwrapped = ko.unwrap(value);
        $(element).Bigdate_Selector().setdate_seconds(valueUnwrapped);
    }, update: function(element, valueAccessor) {
        var value = valueAccessor();
        var valueUnwrapped = ko.unwrap(value);
        if ($(element).Bigdate_Selector().getdate_seconds() != valueUnwrapped)
            $(element).Bigdate_Selector().setdate_seconds(valueUnwrapped);
    }
};

// The number of seconds from zero given the date and time components
function fromcomps(c) {

    // Extract the individual components
    year = c[0];
    month = c[1];
    day = c[2];
    hour = c[3];
    minute = c[4];
    sec = c[5];
    sign_ = c[6];
    sec_in_year = 31536000;
    sec_in_day = 86400;
    sec_in_hour = 3600;
    sec_in_minute = 60;

    ldays = leapdays(0, year);

    daycumsum = _.map([0, 30, 58, 89, 119, 150, 180, 211, 242, 272, 303, 333, 364], function (x) {
        return x + 1;
    });
    daycumsumL = _.map([0, 30, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365], function (x) {
        return x + 1;
    });
    daycumsum[0] = 0;
    daycumsumL[0] = 0;

    // Counter for the passed seconds
    d = 0;

    // Add the seconds from the normal years
    d = d + sec_in_year * year;

    // Add the seconds from the leap years
    d = d + sec_in_day * ldays;

    // Add the days of the months for a leap year and a normal year
    D = isleap(year) && month > 0 ? daycumsumL : daycumsum;

    d = d + sec_in_day * D[month];
    d = d + sec_in_day * day;

    // Add the time components
    d = d + sec_in_hour * hour;
    d = d + sec_in_minute * minute;
    d = d + sec;

    return d * sign_;

}

// The date and time components from the number of seconds from zero
function intocomps(d) {

    // The input is a raw number of passed seconds. The number already contains
    // the seconds from the leap days.

    var sign_ = sign(d);
    daycumsum = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365];
    daycumsumL = [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366];


    d = Math.max(d, -d);

    // The number of seconds in a normal year
    sec_in_year = 31536000;
    sec_in_day = 86400;
    sec_in_hour = 3600;
    sec_in_minute = 60;

    // Calculate an approximation of the year 
    var year = intdiv(d, sec_in_year);

    // Remove the seconds of the approximated year
    d = d - year * sec_in_year;

    // Remove the unaccounted leap days that were not considered in the year estimate
    d = d - leapdays(0, year) * sec_in_day;

    // See if the unaccounted leap days changed the year
    if (d < 0) {

        // Calculate the number of required years to compensate the leap days
        negyeard = (intdiv((d * -1), sec_in_year)) + 1;

        // Remove the destroyed years
        year = year - negyeard;

        // Change the years into seconds
        d = d + (sec_in_year * negyeard);

        // Add the leap days from the sacrificed years
        d = d + leapdays(year, year + negyeard) * sec_in_day;

        // See if the leap days add years. Keep adding the years until all of the
        // packs of 365/366 leap days are used.
        while (true) {

            // If the current year is a leap year and the previous compensation gave more
            // than 365 days, then we need to change the year.
            if ((intdiv(d, sec_in_day) >= 366) && (isleap(year))) {
                year = year + 1;
                d = d - (366 * sec_in_day);
                continue;
            }
            // Same procedure but with the current year as a normal year
            if ((intdiv(d, sec_in_day) >= 365) && (!isleap(year))) {
                year = year + 1;
                d = d - (365 * sec_in_day);
                continue;
            }
            break;
        }
    }

    var day = intdiv(d, sec_in_day);
    d = d % sec_in_day;

    D = !isleap(year) ? daycumsum : daycumsumL;

    // Determine the correct month considering that the current year might be a leap year
    // The month is found using a cumulative sum
    var month = 1;
    var days = 0;

    for (var index = 0; index < 13; index++) {
        days = D[index];
        if (index === 0) continue;
        if ((day >= D[index - 1]) && (day < days)) {
            // We found the current month. Remove the days that are already accounted
            // with the previous months of the current year.
            month = index - 1;
            day = day - D[index - 1];
            break;
        }
    }

    // Acquire the time components
    var hour = intdiv(d, sec_in_hour);
    d = d % sec_in_hour;

    var minute = intdiv(d, sec_in_minute);
    d = d % sec_in_minute;

    r_ = [year, month, day, hour, minute, d, sign_];

    return r_;


}

// http://stackoverflow.com/questions/16353211/check-if-year-is-leap-year-in-javascript

function isleap(year) {
    return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
}

function intdiv(a, b) {
    return Math.floor(a / b);
}

function sign(val) {
    return val > 0 ? 1 : -1;
}

function leapdays(x, y) {
    x = x - 1;
    y = y - 1;
    return (intdiv(y, 4) - intdiv(x, 4)) - (intdiv(y, 100) - intdiv(x, 100)) + (intdiv(y, 400) - intdiv(x, 400));
}

function compare_bigdates(x1,x2){
    if (x1 < x2)
        return -1;
    else if (x1 == x2)
        return 0;
    else
        return 1;
}

function string_from_bigdate(d){
    var c = d;

    if (typeof(d) === "number")
        c = intocomps(d);

    var year_text = c[0];
    var sign_ = c[6];
    if (sign_ == -1) year_text = year_text + " BCE";
    return (parseInt(c[2], 10) + 1) + "." + (parseInt(c[1], 10) + 1) + "." + year_text + " " + c.slice(3, 6).join(":");
}

function Bigdate_Selector() {

    // The current time that is used to parse the date and UI components
    var current = 0;
    var UIcomponents = null;
    var dateDisplay = null;
    var element = null;
    var id = 0;
    var opts = {};
    var change_callback = null;

    this.getrootelement = function () {
        return this.element;
    };

    this.setdate_components = function (c) {
        this.current = fromcomps(c);
        this.viewdate(c);
        return this;
    };

    this.setdate_seconds = function (d) {
        this.current = d;
        this.viewdate(intocomps(this.current));
        return this;
    };

    this.getdate_seconds = function () {
        return this.current;
    };

    this.adjust_delta = function (x) {
        this.current += x;
        this.viewdate();
        return this;
    };

    this.getdate_components = function () {
        return intocomps(this.current);
    };

    this.readdate = function () {
        var c = _.map(this.UIcomponents, function (x) {
            return parseInt(x.val(), 10);
        });
        c[1] -= 1;
        c[2] -= 1;
        c.push(sign(c[0]));
        c[0] = Math.max(-c[0], c[0]);
        this.current = fromcomps(c);
    };

    this.viewdate = function (c) {
        if (c === undefined || c === null) c = this.current;

        if (typeof (c) == "number") c = intocomps(c);

        var sign_ = c[6];
        _.each(_.zip(this.UIcomponents, c), function (e, i) {
            if (i != 6) {
                if (i === 0 && sign_ == -1) e[0].val("-" + e[1].toString());
                else if (i > 0 && i < 3) e[0].val(e[1] + 1);
                else e[0].val(e[1]);
            }
        }, this);

        if (this.opts.datedisplay) {
            this.dateDisplay.val(string_from_bigdate(c));
        }
    };

    this.reset_callback = function(new_func){
        this.change_callback = new_func;
        return this;
    };

    this.call_callback = function(){
        if (this.change_callback !== null){
            this.change_callback(this.getdate_seconds());
        }
    };

    this.install = function(e,opts){

        this.opts = $.extend({
            datedisplay: false,
            initial_current: 64558522061,
            change_callback: null,
            captions: true
        }, opts);

        this.change_callback = this.opts.change_callback;
        this.current = this.opts.initial_current;
        this.element = e;
        this.element.html("");
        this.UIcomponents = [];

        Bigdate_Selector.prototype.instances++;
        this.id = Bigdate_Selector.prototype.instances;
        e.data("id_number", this.id);

        var elInit = [{
            value: "2014",
            width: "80px",
            scrollboost: 10,
            dtype: "year",
            maxval: 14000000000,
            minval: -14000000000,
            lab: "Y"
        }, {
            value: "1",
            width: "32px",
            scrollboost: 1,
            dtype: "month",
            maxval: 12,
            minval: 1,
            lab: "M"
        }, {
            value: "1",
            width: "32px",
            scrollboost: 1,
            dtype: "day",
            maxval: 31,
            minval: 1,
            lab: "D"
        }, {
            value: "1",
            width: "32px",
            scrollboost: 1,
            dtype: "hour",
            maxval: 23,
            minval: 0,
            lab: "h"
        }, {
            value: "1",
            width: "32px",
            scrollboost: 1,
            dtype: "minute",
            maxval: 59,
            minval: 0,
            lab: "m"
        }, {
            value: "1",
            width: "32px",
            scrollboost: 2,
            dtype: "second",
            maxval: 59,
            minval: 0,
            lab: "s"
        }];

        // Build UI components
        _.each(elInit, function (e, i) {
            var uiElem = $("<input type=\"number\" />");
            uiElem.addClass("bdS_selCtrl", "input");
            uiElem.attr({
                "min": e.minval,
                    "max": e.maxval
            });
            uiElem.attr("id", "bigdateUIcomp_" + e.dtype + this.id);

            uiElem.val(e.value);
            uiElem.css({
                "width": e.width,
                    "text-align": "center",
                    "border": 0
            });

            var this_ = this;
            uiElem.change(function () {

                var newval = parseInt(this.value);

                if (e.dtype !== "day") {

                    if (newval > e.maxval) {
                        newval = e.maxval;
                        this.value = newval;
                    }
                    if (newval < e.minval) {
                        newval = e.minval;
                        this.value = newval;
                    }

                } else {
                    var month_ = parseInt($("#bigdateUIcomp_month" + this_.id).val(), 10);
                    var isleap_ = isleap(parseInt($("#bigdateUIcomp_year" + this_.id).val(), 10));

                    if (((newval) > Bigdate_Selector.prototype.days_in_months[month_] && !isleap_ && month !== 1) || (isleap_ && month === 1 && (newval) > 29)) {
                        this.value = Bigdate_Selector.prototype.days_in_months[month_];
                    }
                    if (newval < 0) this.value = 0;
                }

                this_.readdate();
                this_.viewdate();

                if (this_.change_callback !== null) this_.change_callback(this_.current);
            });

            uiElem.bind("mousewheel", function (event, delta) {
                this.value = parseInt(this.value, 10) + (Math.max(delta, -delta) == 1 ? delta : delta * e.scrollboost);

                var newval = parseInt(this.value, 10);

                if (e.dtype !== "day") {

                    if (newval > e.maxval) {
                        newval = e.maxval;
                        this.value = newval;
                    }
                    if (newval < e.minval) {
                        newval = e.minval;
                        this.value = newval;
                    }

                } else {
                    var month_ = parseInt($("#bigdateUIcomp_month" + this_.id).val(), 10);
                    var isleap_ = isleap(parseInt($("#bigdateUIcomp_year" + this_.id).val(),10));

                    if (((newval) > Bigdate_Selector.prototype.days_in_months[month_] && !isleap_ && month !== 1) || (isleap_ && month === 1 && (newval) > 29)) {
                        this.value = Bigdate_Selector.prototype.days_in_months[month_];
                    }
                    if (newval < 0) this.value = 0;
                }

                this_.readdate();
                this_.viewdate();

                if (this_.change_callback !== null) this_.change_callback(this_.current);

                return false;
            });

            uiElem.filter_input({
                regex: '[-0-9]'
            });

            this.UIcomponents.push(uiElem);

            var cont_div = $("<div />").css({
                "display": "block-inline",
                    "float": "left"
            });

            if (this.opts.captions) {
                cont_div.append(
                $("<label/>").attr(
                    "for", "bigdateUIcomp_" + e.dtype + this.id).css({
                    "text-align": "center"
                }).html(e.lab));
            }

            cont_div.append(uiElem);

            this.element.append(cont_div);


        }, this);

        if (this.opts.datedisplay) {

            var cont_div = $("<div />").css({
                "display": "block-inline",
                    "float": "left"
            });

            if (this.opts.captions) {
                cont_div.append(
                $("<label/>").attr(
                    "for", "bigdateUIcompDateDisplay_" + e.dtype + this.id).css({
                    "text-align": "center"
                }).html("Resulting date and time"));
            }

            this.dateDisplay = $("<input type=\"text\" />").css({
                "enabled": "0",
                    "width": "150px",
                    "text-align": "center"
            }).attr("id", "bigdateUIcompDateDisplay_" + e.dtype + this.id);
            cont_div.append(this.dateDisplay);

            this.element.append(cont_div);
        }

        this.element.append($("<div />").css("clear", "both"));

        Bigdate_Selector.prototype.registered.push(this);

        this.viewdate();

    };

}

Bigdate_Selector.prototype.instances = 0;
Bigdate_Selector.prototype.registered = [];
Bigdate_Selector.prototype.days_in_months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

$.fn.extend({
    Bigdate_Selector: function (opts) {
        var e = $(this);
        var selector = null;
        if (e.data("Bigdate_Selector_initialized") === undefined) {
            selector = new Bigdate_Selector();
            selector.install(e, opts);
            e.data("Bigdate_Selector_initialized", "done");
        } else {

            if (Bigdate_Selector.prototype.registered.length == 1)
                selector = Bigdate_Selector.prototype.registered[0];
            else {
                var index_ = _.indexOf(_.map(Bigdate_Selector.prototype.registered, function (x) {
                    return x.id;
                }), e.data("id_number"));
                selector = Bigdate_Selector.prototype.registered[index_];
            }

        }
        return selector;
    }
});