/*
Get a new event template for editing
 */
function eventNewEmptyTemplate(){

    return {
        author: logged_id,
        id : -1,
        content: 'New event', // The title of new event
        start: 0,
        end: 1,
        significance: 1,
        wikilink: '',
        description: 'This is a new event. Please write an appropriate description for the event, because it helps users to find events using the search functionality.',
        data: [],
        filename: '',
        categories: [],
        visibility_type: "0",
        incertitude : 0,
        incertitude_type : 0
    };
}

/*
Load event for rendering event information popup or event editing widget
 */
function preLoadEvent(x){
    console.log("Preloading the following event:");
    console.log(x);
	eventJSON = x;
    editedEvent = eventJSON.id;
    var needed_units = _.uniq(_.pluck(x.data,'eventunit'));
    fetchNeededUnits(needed_units);
}

/*
Render modal event information popup window
 */
function eventInfo(x){
	// Hide possible edit view and restore map size
	eventModel.cancel();

    preLoadEvent(x);

	// Check that we have the needed units loaded
	eventModel.setData();
    $("div#eventIModal").modal();
}

/*
Add an event unit into event editor selected units. The function
can be used as a callback to eventUnitFinder_FindUnits.
 */
function eventEditor_addSelectedUnit(evUnit){
    if (evUnit != undefined){
        eventModel.addUnit(evUnit.id);
	}
}

var EventViewModel = function()
{
	var self = this;

    self.incertitude = ko.observable(eventJSON.incertitude);
    self.incertitude_type = ko.observable(eventJSON.incertitude_type);

    self.visibility_type = ko.observable(eventJSON.visibility_type);
    self.private_event = ko.computed(function(){
        return self.visibility_type() == "3";
    });

    self.categories = ko.observableArray(eventJSON.categories);
    self.author = ko.observable(eventJSON.author);
	self.content = ko.observable(eventJSON.content);
	self.wikilink = ko.observable(eventJSON.wikilink);
	self.description = ko.observable(eventJSON.description);
	self.start = ko.observable(eventJSON.start);
	self.end = ko.observable(eventJSON.end);
	self.significance = ko.observable(eventJSON.significance);
	self.filename = ko.observable(eventJSON.filename);
	self.id = ko.observable(eventJSON.id);

	self.data = ko.observableArray([]);
	self.selectedItem = ko.observable();

    self.start_string = ko.computed(function(){
        return string_from_bigdate(self.start());
    });

    self.end_string = ko.computed(function(){
        return string_from_bigdate(self.end());
    });

	self.newEvent = function(){
		eventJSON = eventNewEmptyTemplate();

        // editedEvent refers to the ID of the event
		editedEvent = -1;
		self.getData();
	};

    self.description_markdown = ko.computed(function(){
        return marked(self.description());
    });

	self.setData = function()
	{
        self.visibility_type(eventJSON.visibility_type);
        self.categories(eventJSON.categories);
		self.content(eventJSON.content);
		self.wikilink(eventJSON.wikilink);
		self.description(eventJSON.description);
        self.incertitude(eventJSON.incertitude);
        self.incertitude_type(eventJSON.incertitude_type);

        console.log(eventJSON);

		self.start(eventJSON.start);
		self.end(eventJSON.end);

		self.significance(eventJSON.significance);
		var data = $.map(eventJSON.data, function(unit){ return new eventUnit(unit); });
		self.filename(eventJSON.filename);
        self.author(eventJSON.author);
		self.data(data);
		self.id(eventJSON.id);
	};

	self.getData = function(x)
	{
		if(x != null && x != undefined && typeof(x) == "object"){
			// Check that we have the needed units loaded
        	var needed_units = _.uniq(_.pluck(x.data,'eventunit'));
        	fetchNeededUnits(needed_units);
			eventJSON = x;
		}
		vectors.removeAllFeatures();
		self.setData();
		vectors.events.remove("featureadded");

        // Force the preset topic checkboxes to be unchecked
        $(".preset_topic_selector_class_event_editor").removeAttr("checked");

		$(".editorView").hide();
		$("#eventEditor").show();
    	$("#openlayers_map").addClass("adminMap");

        selectedCategories.clear();
        $("#eEditor_SelCats").html('');
        var alreadySelectedCategory = '<a class="category_enumeration_Anchor" id="catSelectedButton_{{ id }}">{{ title }}</a>';

        _.each(eventJSON.categories, function(x){

            // Add possible preset categories
            var fixed_title = x.title.replace(" ","_");
            var element_query = $("#preset_topic_selector_event_editor__"+fixed_title);
            if (element_query.length == 1)
                element_query.attr("checked","checked");

            if (selectedCategories.pushUnique(x)){

                var removeCategoryButton = $(Mustache.render(alreadySelectedCategory, x));

                removeCategoryButton.click(function(){

                    selectedCategories.removeById(x.id);

                    $(this).hide();
                });

                $("#eEditor_SelCats").append(removeCategoryButton);
            }
        });

	};

	var drawPoints = function(){

		vectors.removeAllFeatures();
		var featureId = 0;

		ko.utils.arrayForEach(self.selectedItem().datapoints(), function(point){

			var feature_point = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(point.x, point.y));
			var label_point = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(point.x, point.y));

        	feature_point.id = "route_id_"+featureId;
        	label_point.id = "label_id_"+featureId;

            label_point.style = {label : ""+featureId };

        	featureId++;

        	vectors.addFeatures([feature_point]);
            vectors.addFeatures([label_point]);

		});
	};

	self.unitRoute = function(unit)
	{
		$(".unitInfo").hide();
		$(".unitRoute").show();
		self.selectedItem(unit);
		drawPoints();
		click.activate();
	};

	self.removeRoutePoint = function( point ){
		self.selectedItem().datapoints.remove(point);
		drawPoints();
	};

	self.deleteEventAndClose = function(){
		self.deleteEvent();
        self.cancel();
	};

	self.deleteEvent = function(){

		var data = {
			id : self.id()
		};

		$.ajax({
			url : eventDBDeleteUrl,
			data : data,
			method : "POST",
			success : function(value){
				console.log("<eventdelete> Ajax response: "+value+", for id "+self.id());
                removeSingleEvent(self.id());
			}
		});

	};

	self.save = function()
	{
		if(editedEvent != -1 && editedEvent != null){

            // Find the index of the event in the array of all loaded events using event ID
            var editedEventIndex = currentlyLoadedEvents.findIndexById(editedEvent);

			for(var key in currentlyLoadedEvents[editedEventIndex])
			{
				if(self.hasOwnProperty(key) && ko.isObservable(self[key]) && currentlyLoadedEvents[editedEventIndex].hasOwnProperty(key))
				{
					currentlyLoadedEvents[editedEventIndex][key] = ko.toJS(self[key]);
				}
			}

		}

		vectors.removeAllFeatures();
		$("#eventEditor").hide();
    	$("#openlayers_map").removeClass("adminMap");

        // Determine the selected preset topics as a lit
        var selected = _.map($(".preset_topic_selector_class_event_editor:checked"), function(x){
            return x.id;
        });

    	var params = {
			id : self.id(),
			title : self.content(),
			wiki : self.wikilink(),
			start : self.start(),
			stop : self.end(),
			desc : self.description(),
			data : ko.toJSON(self.data()),
			sig : self.significance(),
        	filename : self.filename(),
            visibility_type : self.visibility_type(),
            incertitude : self.incertitude(),
            incertitude_type : self.incertitude_type(),
            selcats : ko.toJSON(_.pluck(selectedCategories,'id')),
            selected_preset_topics : selected.join(";")
		};

		var url = (self.id() == -1) ? eventDBCreateUrl : eventDBUpdateUrl;

        // Update the currently loaded and used event if we are updating an event
        if (self.id() > -1){
            var evIndex = currentlyLoadedEvents.findIndexById(self.id());
            if (evIndex != undefined && evIndex > -1){
                currentlyLoadedEvents[evIndex].data = ko.toJS(self.data());
                currentlyLoadedEvents[evIndex].title = self.content();
                currentlyLoadedEvents[evIndex].content = self.content();
                currentlyLoadedEvents[evIndex].description = self.description();
                currentlyLoadedEvents[evIndex].visibility_type = self.visibility_type();
                currentlyLoadedEvents[evIndex].start = self.start();
                currentlyLoadedEvents[evIndex].end = self.end();

                // Fetch the new movement coordinates for rendering
                setupDataForCoordinates();
            }
        }

		$.ajax({
			url : url,
			method : "POST",
			data : params,
			success : function(data){
				console.log("<saveEvent> $.ajax applied success function with returned data: "+data);

     	       // Close the event editing view
       	     $("#eventEditor").hide();
       	     $("#openlayers_map").removeClass("adminMap");
			}
		});
	};

	self.cancel = function(){
		$("#eventEditor").hide();
		$("#openlayers_map").removeClass("adminMap");
	};

	self.saveRoute = function(){
		vectors.removeAllFeatures();
		click.deactivate();
		dragFeature.deactivate();
		$(".unitRoute").hide();
		$(".unitInfo").show();
	};

    self.authorName = ko.computed(function(){

        if (self.author() == logged_id || self.author()==-1)
            return "You!";

        var targetVal = "";
        $.ajax({
			url : nameQueryURL,
			method : "POST",
			data : {author:self.author()},
            async : false,
			success : function(data){
                targetVal = data;
			}
		});
        return targetVal;
    });

	self.addUnit = function(id){
		var needed_units = [];
		needed_units.push(id);
		fetchNeededUnits(needed_units);
		self.data.push(new eventUnit({
            "eventunit":id,
            "datapoints" : [],
            "unit_label":'',
            'zoom_min' : 1,
            'zoom_max' : 10
        }));
	};

    self.originalAuthor = ko.computed(function(){
        return this.author() > 0 && logged_id != null && this.author() == logged_id;
    },this);

    self.updateMode = ko.computed(function(){
        return this.id() > 0;
    },this);

	self.deleteUnit = function(unit){
		self.data.remove(unit);
	};

	self.route = function(x,y)
	{

        var final_date = null;
        if (self.selectedItem().datapoints().length == 0){
            final_date = 0;
        } else {
            final_date = _.last(self.selectedItem().datapoints()).time();
        }

		self.selectedItem().datapoints.push(
            new Datapoint({
                "x":x,
                "y":y,
                "time":final_date
            })
        );
        drawPoints();
	};

	self.routeDates = function(element,index,data){

        // 1) Initialize a new selector for the big dates
        // 2) Register a callback that changes the time in the selected point of the active routing
        // 3) Change the current time of the new selector
        $(element).Bigdate_Selector().reset_callback(function(t){
            data.time(t);
        }).setdate_seconds(data.time()).call_callback();

	};

};

/*
Search events that can be added inside a playlist.
 */
function event_SearchForPlaylisteditor(){
    selectedCategories.clear();
    combinedSearch(['unit','playlist','layer'], function(X){
        selectedPlaylist.data.pushUnique(X);
    });
}

/*
Render a small template for event information using Mustache.
 */
function event_renderSmallInformation(event_,fromPlaylistEditor){

    // If we acquire an empty object, then return empty string
    if (event_ === null || typeof(event_) !== "object" || _.isEmpty(event_))
        return "";

    event_.delLink = fromPlaylistEditor;

    var template = '<div style="clear:both;">\
    <h4>\
    {{#delLink}}\
    <button id="remSelPlayEvID{{id}}"><i class="fa fa-times"></i></button>\
    {{/delLink}}\
    {{content}}</h4>\
    <p><img src="{{ filename }}" style="max-height: 100px;max-width: 100px;float: left;margin: 7px;" /> {{ description }}</p>\
    </div>';

    var renTemp = $(Mustache.render(template, event_));

    $(renTemp).first("#remSelPlayEvID"+event_.id).click(function(){
        selectedPlaylist.data.removeById(event_.id);
        playlist_EditorRenderEvents();
    });

    return renTemp;
}

/*,
Render a template for event information using Mustache.
 */
function renderEventTemplate(data){

    data.limitLength = function(){
        return function(text, render){
          return render(text).substr(0,50) + '...';
        }
      };

    data.render_big_date_string = function(){
        return function(text, render){
            return string_from_bigdate(parseInt(render(text)));
        }
    }

    var incert_cols = ['black','#660066','red','#FF6600','#FFCC66','green'];
    data.incertitude_icon = $("<span></span>").text("?").css({
        'font-size':18-data.incertitude,
        'color':incert_cols[data.incertitude]
    })[0].outerHTML;

    var template = '<tr>' +
        '<td>Event</td>' +
        '<td><img src="{{ filename }}" style="max-height: 50px;max-width: 50px;" /></td>' +
        '<td>{{ title }}</td>' +
        '<td>{{#limitLength}}{{ description }}{{/limitLength}}</td>' +
        '<td><button class="btn btn-primary" id="selSelect{{ id }}" style="margin-bottom: 6px;margin-right:10px;">Select</button></td>' +
        '<td>{{{ repeatSignificanceStars }}}</td>' +
        '<td>{{#render_big_date_string}}{{ start }}{{/render_big_date_string}}</td>' +
        '<td>{{#render_big_date_string}}{{ end }}{{/render_big_date_string}}</td>' +
        '<td>{{{incertitude_icon}}}</td>' +
        '</tr>';

    return Mustache.render(template,data);

}

/*
Remove a single event that is currently loaded and initialized for the main view. The functionality allows
used to manage a large collection of simultaneous events, without forcing to reload the whole page.
 */
function removeSingleEvent(eID){
    var eventObject = _.find(currentlyLoadedEvents,function(x){ return x.id == eID; });
    if (eventObject){
        currentlyLoadedEvents.removeById(eventObject.id);
        $("#eventSelectionAnchor"+eventObject.id).remove();
        setupDataForCoordinates();
    }
}

/*
Render a selection anchor for event using Mustache. Optionally, a context menu can be supplied with the
event anchor for offering convenient methods.
 */
function event_renderSelectionAnchor(event_,showContextMenu_){

    var showContextMenu = true;
    if (showContextMenu_ != undefined && showContextMenu_ != null)
        showContextMenu  = showContextMenu_;

    // Check for events that are already in a playlist
    if (showContextMenu){
        if (_.contains(_.flatten(_.pluck(_.pluck(currentlyLoadedPlaylists,'data'),'IDs')),event_.id)){
            return "";
        }
    }

    var template = '<a id="eventSelectionAnchor{{id}}" href="javascript:void(0);" >' +
        '<img src="{{filename}}" style="max-width:25px;max-height:25px;" /> {{content}}' +
        '<br/>' +
        '</a>';

    var UEl = $(Mustache.render(template, event_));

    UEl.click(function(e){

        if (e.button == 0)
            updateMarkerview(event_.start);

        if (e.button > 0){
            if ($("#cAnchorMenu").length > 0)
                $("#cAnchorMenu").remove();

            var CMenuAdder = $("<div></div>").css({
                'width': '120px',
                'position': 'absolute',
                'left': (e.pageX+3)+'px',
                'top': (e.pageY+10)+'px',
                'z-index': '99999'
            }).attr("id","cAnchorMenu");

            var btnInformation = $('<button class="btn">Information</button>').css('width','118px');
            var btnRemove = $('<button class="btn">Remove</button>').css('width','118px');
            var btnCancel = $('<button class="btn">Cancel</button>').css('width','118px');

            btnInformation.click(function(){
                eventInfo(event_);
                $("#cAnchorMenu").remove();
            });

            btnRemove.click(function(){
                removeSingleEvent(event_.id);
                $("#cAnchorMenu").remove();
            });

            btnCancel.click(function(){
                $("#cAnchorMenu").remove();
            });

            CMenuAdder.append(btnInformation);
            CMenuAdder.append(btnRemove);
            CMenuAdder.append(btnCancel);

            $("body").append(CMenuAdder);

            if(e.preventDefault != undefined)
                e.preventDefault();
            if(e.stopPropagation != undefined)
                e.stopPropagation();

        }

    });

    return UEl;
}

/*
Method for adding a new event category/topic. Currently only available inside an event editor.
 */
function promptNewCategoryAdd(catText){

    if (catText != null && catText != undefined && catText.length > 2){
        bootbox.prompt("Please write a description for the category "+catText, function(result){
            if (result != null && result.length > 0){
                // Now we will add a new category

                $.ajax({
                    url: addCategoryURL,
                    data: {addText:catText,addDesc:result},
                    type: "POST",
                    async : false,
                    success: function(data){
                    }
                });

            }
        });
    }

}

/*
Manage event categories inside a Knockout-model.
 */
function updateEventViewModelForCategories(){
    eventJSON.categories = [];
    _.each(selectedCategories,function(x){
        eventJSON.categories.push(x);
    });
    eventModel.categories(eventJSON.categories);
}

/*
Register category finding functionality and category selection
====
inputSource - jQuery object providing search term in value attribute
allowNewAdding - Allow to add new categories if not found
searchResultContainer - jQuery object where search results are appended
selectedContainer - jQuery object for storing the selected categories
categorySelectionCallback - Call a function when a category is selected
categoryRemovalCallback - Call a function when a previously selected category is removed
 */
function categoryFinder(inputSource,allowNewAdding,searchResultContainer,selectedContainer,categorySelectionCallback,categoryRemovalCallback){

    var catSelectionButton = '<a class="category_enumeration_Anchor" id="catSelectionButton_{{ id }}">{{ title }}</a>';
    var alreadySelectedCategory = '<a class="category_enumeration_Anchor" id="catSelectedButton_{{ id }}">{{ title }}</a>';

    inputSource.unbind('keyup').keyup(function(){

        var searchText = $(this).val();
        var inputLength = searchText.length;

        if (inputLength > 2){

            $.ajax({
                url: categoryDBSearchURL,
                data: {searchText:searchText},
                dataType: "json",
                type: "GET",
                success: function(data){

                    if (data.length == 0){

                        if (allowNewAdding){

                            var catAdderBtn = '<button class="btn">'+searchText+'</button>';
                            catAdderBtn = $(catAdderBtn);
                            catAdderBtn.click(function(){

                                promptNewCategoryAdd(searchText);
                                searchResultContainer.html('');

                            });

                            searchResultContainer.html('<p>No categories found. Register new category</p>');
                            searchResultContainer.append(catAdderBtn);
                        }else
                            searchResultContainer.html('<p>No categories found.</p>');

                    } else {

                        searchResultContainer.html('');

                        // Iterate the results
                        _.each(data,function(U){

                            // Render a category selection button and register a callback
                            var renderedSelectionButton = Mustache.render(catSelectionButton, U);
                            var renderedSelectionButtonElement = $(renderedSelectionButton);
                            renderedSelectionButtonElement.click(function(){

                                if (selectedCategories.pushUnique(U)){

                                    if (categorySelectionCallback != undefined && typeof(categorySelectionCallback) == "function")
                                        categorySelectionCallback(U);

                                    var removeCategoryButton = $(Mustache.render(alreadySelectedCategory, U));

                                    removeCategoryButton.click(function(){

                                        selectedCategories.removeById(U.id);

                                        if (categoryRemovalCallback != undefined && typeof(categoryRemovalCallback) == "function")
                                            categoryRemovalCallback(U);

                                        $(this).hide();
                                    });

                                    selectedContainer.append(removeCategoryButton);

                                }

                            });

                            searchResultContainer.append(renderedSelectionButtonElement);

                        });

                    }

                }
            });

        } else {
            searchResultContainer.html('<p>Start by typing a category name.</p>')
        }

    });

}

/*
If no cached event is found, load event from database and cache. Finally return a loaded or cached event object.
 */
function loadAndReturnSingleEventByID(id){
    var result = null;
    console.log("<loadAndReturnSingleEventByID> Loading event with ID "+id);

    if (id in eventCache){
        console.log("<loadAndReturnSingleEventByID> Event loaded from cache");
        return eventCache[id];
    }

    $.ajax({
        url: this.eventDBSingleQueryURL,
        data: {ID:id},
        dataType: "json",
        async: false,
        success: function(data){
            result = data;
        }
    });

    // Do not allow empty events (= non-existing events)
    if ($.isEmptyObject(result)){
        console.log("<loadAndReturnSingleEventByID> No event found with given ID")
    } else {
        eventCache[result.id] = result;
    }

    return result;
}

/*
Load event from database, a single event. Register callback for first time adding
and callback for every add.
 */
function loadSingleEvent(data,loadedNewCallback,alwaysExecuteCallback){

	if(typeof(data) == "object"){

        // Ensure that only one instance of an event is loaded at any given time, e.g. no duplicates.
        if (currentlyLoadedEvents.pushUnique(data)){

            setupDataForCoordinates();
            updateMarkerview();

            if (loadedNewCallback != undefined && typeof(loadedNewCallback) == "function"){
                loadedNewCallback(data);
            }

        }

        if (alwaysExecuteCallback != undefined && typeof(alwaysExecuteCallback) == "function"){
            alwaysExecuteCallback(data);
        }

	}
    else if (data != undefined && data != null){

        var eventData = null;

        // Cached event loading
        if (data in eventCache){
            eventData = eventCache[data];
        } else {
            eventData = loadAndReturnSingleEventByID(data);
        }

        // Check if we actually found an event
        if (!$.isEmptyObject(eventData)){

            // Do not allow duplicate loaded events
            if (currentlyLoadedEvents.pushUnique(eventData)){

                setupDataForCoordinates();

                if (loadedNewCallback != undefined && typeof(loadedNewCallback) == "function"){
                    loadedNewCallback(eventData);
                }

            }

            if (alwaysExecuteCallback != undefined && typeof(alwaysExecuteCallback) == "function"){
                alwaysExecuteCallback(eventData);
            }

        }

    }

}

function toggleEventsTabVisibility(){
    var iconEl = $("#eventContentTabsControls_icon");
    var toolbarEl = $("#eventContentTabs");
    var finalClass = iconEl.attr("class", (iconEl.attr("class")=="icon-minus") ? "icon-plus" : "icon-minus").attr("class");
    if (finalClass == "icon-plus")
        toolbarEl.hide("slow");
    else
        toolbarEl.show("slow");
}

function yourContent_ModelPaneElement(M){
    if (M && M != undefined && M != null && _.has(M,"title")){

        var el_ = $("<a></a>").text(
            (M.title.length < 15) ? M.title : M.title.slice(0,15)+"..."
        ).attr("href","javascript:void(0);");

        var removalURL = "";

        // Context-specific actions for clicking the model name
        if (M.modeltype == 'event'){
            el_.click(function(){ loadSingleEvent(M.id); });
            removalURL = eventDBDeleteUrl;
        }

        if (M.modeltype == 'unit'){
            el_.click(function(){ unitInfo(M); });
            removalURL = unitDBDeleteUrl;
        }

        if (M.modeltype == 'playlist'){
            el_.click(function(){ loadPlaylist(M); });
            removalURL = playlistDBDeleteUrl;
        }

        if (M.modeltype == 'layer'){
            el_.click(function(){ layerModel.newLayer(M); });
            removalURL = layerDBDeleteUrl;
        }

        el_.attr("title", (M.description.length < 80 ? M.description : M.description.slice(0,80)+"..." ));

        var resultingContainerID = "yourContent_"+M.modeltype+"_"+M.id;

        var editButton = $('<button><i class="fa fa-pencil-square-o"></i></button>');
        var containerDiv = $('<div></div>').attr("id",resultingContainerID);
        var deleteButton = $('<button><i class="fa fa-times"></i></button>');

        editButton.css({"margin-right":"5px"});

        deleteButton.click(function(){

            // Ask for removal confirmation
            bootbox.confirm("Confirm deletion of '"+ M.title +"'", function(result) {
                if (result){

                    // Remove the data model from server database
                    $.ajax({
                        url : removalURL,
                        data : { id: M.id, lid : M.id },
                        method : "POST",
                        success : function(responseData){
                            /*
                            Update the user interface and loaded models if the removal was successful.
                            For example, if the deleted model was a loaded event or playlist, then the
                            event or playlist+events are unloaded from user. Therefore, one experiences a more
                            fluid and dynamic use experience.
                             */
                            if (M.modeltype == 'event')
                                removeSingleEvent(M.id);
                            if (M.modeltype == 'playlist')
                                removeSinglePlaylist(M.id);
                        }
                    });

                    // Notify the user about the removal
                    $.notifyBar({
                        html: "Deleted '"+ M.title +"'",
                        delay: 3000,
                        animationSpeed: "normal",
                        position: "bottom"
                    });

                    // Hide the UI element containing the model title
                    $("#"+resultingContainerID).hide();

                }
            });

        });

        // Context-specific actions for clicking the edit button
        editButton.click(function(){
            if (M.modeltype == "playlist")
                playlist_OpenEditor(M);
            if (M.modeltype == "event"){
                preLoadEvent(M);
                eventModel.getData();
            }
            if (M.modeltype == "unit"){
                unitModel.init(M);
                unitEdit();
            }
            if (M.modeltype == "layer")
                layerModel.newLayer(M);
        });

        containerDiv.append([deleteButton, editButton, el_]);

        return containerDiv;
    }
    return "";
}

function open_yourContent(){
    $.ajax({
        url : eventYourContentQuery,
        method : "POST",
        success : function(data){

            data = $.parseJSON(data);

            $("#yourContentIModal_eventsPane").html('');
            $("#yourContentIModal_layersPane").html('');
            $("#yourContentIModal_playlistsPane").html('');
            $("#yourContentIModal_unitsPane").html('');

            _.each(data,function(x){
                $("#yourContentIModal_"+ x.modeltype +"sPane").append(yourContent_ModelPaneElement(x));
            });

            userModel.cacheValues();

            $("div#yourContentIModal").modal();
        }
    });
}