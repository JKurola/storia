/*
Get a new unit template for editing.
*/
function unitNewEmptyTemplate(){

	return {
		id : -1,
		author : logged_id,
		title : '',
		description : '',
		disappears : 1,
		filename : '',
		appears : 0,
		location : '',
		wikilink : '',
        imageData : { north:'',east:'',south:'',west:'',north_west:'',north_east:'',south_west:'',south_east:''}
	};

}

function unitEdit()
{
    $("#unitCooperationEditor").unbind("click").click(function(){
        open_collaborator_editor(unitModel.id(),1);
    });
	$("#unitEModal").modal();
}

/*
A single datapoint for routing units inside events. Therefore, a collection of data points defines a route for
any given unit.
 */
function Datapoint(data)
{
	this.x = parseFloat(data.x);
	this.y = parseFloat(data.y);
    this.time = ko.observable(data.time);
}

/*
Create new Knockout data model for unit inside event, the format follows the specified JSON for data inside events.
 */
function eventUnit(data)
{
    this.zoom_max = ko.observable(data.zoom_max);
    this.zoom_min = ko.observable(data.zoom_min);
    this.unit_label = ko.observable(data.unit_label);
	this.eventunit = ko.observable(data.eventunit);
	this.image = ko.computed(function(){
		return unit_data[data.eventunit].filename;
	});
    this.unit_title = ko.computed(function(){
        return unit_data[data.eventunit].title;
    });
	this.datapoints = ko.observableArray([]);
	var points = $.map(data.datapoints, function(item){ return new Datapoint(item); });
	this.datapoints(points);
}

var UnitViewModel = function()
{
	var self = this;
	this.id = ko.observable(unitJSON.id);
	this.filename = ko.observable(unitJSON.filename);
	this.author = ko.observable(unitJSON.author);
	this.title = ko.observable(unitJSON.title);
	this.description = ko.observable(unitJSON.description);
	this.location = ko.observable(unitJSON.location);
	this.wikilink = ko.observable(unitJSON.wikilink);
	this.appears = ko.observable(unitJSON.appears);
	this.disappears = ko.observable(unitJSON.disappears);
    this.imageData = ko.observable(unitJSON.imageData);

    self.new_unit = function(){
        unitModel.init(unitNewEmptyTemplate());
        $("#unitEModal").modal();
    };

    self.appears_string = ko.computed(function(){
        return string_from_bigdate(self.appears());
    });

    self.disappears_string = ko.computed(function(){
        return string_from_bigdate(self.disappears());
    });

	self.mode = ko.computed(function(){
		console.log("<KO: UnitViewModel> ID in self.mode = "+self.id());
		return (self.id() == -1 || self.id() == null) ? "Save" : "Update";
	});

    self.originalAuthor = ko.computed(function(){
        return this.author() > 0 && logged_id != null && this.author() == logged_id;
    },this);

    self.updateMode = ko.computed(function(){
        return this.id() > 0;
    },this);

    self.validatedTitle = ko.computed(function(){
        if (this.title().length == 0)
            return "New unit";
        else
            return this.title();
    },this);

	self.init = function(x){

		if(x != null && typeof(x) == "object")
			unitJSON = x;

		eventModel.cancel();

		for(var key in unitJSON){
			if(self.hasOwnProperty(key) && unitJSON.hasOwnProperty(key))
				self[key](unitJSON[key]);

		}
	};

	self.unitSave = function(){

		var url = (self.id() == -1) ? unitDBCreateUrl : unitDBUpdateUrl;

		var data = {
			id : self.id(),
			title : self.title(),
    		location : self.location(),
    		wikilink : self.wikilink(),
    		desc : self.description(),
        	filename : self.filename(),
    		start : self.appears(),
    		stop : self.disappears(),
            imageData : ko.toJSON( self.imageData() )
    	};

    	$.ajax({
    		url : url,
    		data : data,
    		method : "POST",
    		success : function(data){
    			console.log("<unitSave> Ajax returned success data: "+data);
    			$("#unitEModal").modal('hide');
    		}
    	});

	};

	self.unitDelete = function(){

		var data = {
			id : self.id
		};

		$.ajax({
			url : unitDBDeleteUrl,
			data : data,
			method : "POST",
			success : function(data){
				console.log("<unitDelete> Ajax returned success data: "+data+", for id "+self.id());
				$("#unitEModal").modal('hide');
			}
		});

	};

};

/*
Fetch a collection of needed units from the server. The units are used inside events, and any loaded
unit is cached for later reuse. However, browser reload empties the cache. One could cache the units
using HTML5 local storage, but the current solution works well OK for now.
 */
function fetchNeededUnits(req){

    // Select units that are not found in cache
    unitsNotFound = _.filter(req, function(x){return !(x in unit_data);});

    if (unitsNotFound.length > 0){

        console.log("<fetchNeededUnits> Fetching unit with ID "+unitsNotFound);
        $.ajax({
            async: false,
            url: UnitURL,
            dataType: "json",
            method: "POST",
            data: {
                "selected_units": ko.toJSON( unitsNotFound )
            },
            success: function(data) {
                console.log("<fetchNeededUnits> Received response:");
                console.log(data);
                _.each(data, function(uData){
                    unit_data[uData.id] = uData;
                });
            }
        });

    }
}

/*
Render modal unit information popup window
 */
function unitInfo(x){
    console.log(x);

	unitModel.init(x);

    $("div#unitIModal").modal();
}

/*
Render an unit information template using Mustache. The supplied data is an actual Unit data object.
 */
function renderEventUnitTemplate(data){

    data.limitLength = function() {
        return function(text, render) {
          return render(text).substr(0,50) + '...';
        }
      };

    data.render_big_date_string = function(){
        return function(text, render){
            return string_from_bigdate(parseInt(render(text)));
        }
    }

    var template = '<tr>' +
        '<td>Unit</td>' +
        '<td><img src="{{ filename }}" style="max-height: 50px;max-width: 50px;" /></td>' +
        '<td>{{ title }}</td>' +
        '<td>{{#limitLength}}{{ description }}{{/limitLength}}</td>' +
        '<td><button class="btn btn-primary" id="selSelect{{ id }}" style="margin-bottom: 6px;margin-right:10px;">Select</button></td>' +
        '<td></td>' +
        '<td>{{#render_big_date_string}}{{ appears }}{{/render_big_date_string}}</td>' +
        '<td>{{#render_big_date_string}}{{ disappears }}{{/render_big_date_string}}</td>' +
        '<td></td>' +
        '</tr>';

    return Mustache.render(template, data);

}

// click event for map
OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
    defaultHandlerOptions: {
        'single': true,
        'double': false,
        'pixelTolerance': 0,
        'stopSingle': false,
        'stopDouble': false
    },

    initialize: function(options) {
        this.handlerOptions = OpenLayers.Util.extend(
            {}, this.defaultHandlerOptions
        );
        OpenLayers.Control.prototype.initialize.apply(
            this, arguments
        );
        this.handler = new OpenLayers.Handler.Click(
            this, {
                'click': this.trigger
            }, this.handlerOptions
        );
    },

    trigger: function(e) {
        var lonlat = map.getLonLatFromPixel(e.xy);
        var feature_point = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(lonlat.lon, lonlat.lat));

        feature_point.id = "route_id_"+featureId;
        featureId++;

        vectors.addFeatures([feature_point]);
        bazier.addFeatures([feature_point]);

        eventModel.route(lonlat.lon, lonlat.lat);
    }

});

function activeDrag()
{
	if($("#dragSel").is(":checked")){
		dragFeature.activate();
		click.deactivate();
	}
	else{
		dragFeature.deactivate();
		click.activate();
	}
}