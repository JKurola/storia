/*
Get a new layer template for editing
*/
function layerNewEmptyTemplate()
{
	return{
		author: logged_id,
		id : -1,
		name : "Name of the layer",
		title : "New Layer",
		start : 0,
		stop : 1,
		description : "",
		data : []
	};
}

/*
Knockout model layers.
 */
var LayerViewModel = function()
{
	var self = this;

	self.name = ko.observable(layerJSON.name);
	self.author = ko.observable(layerJSON.author);
	self.id = ko.observable(layerJSON.id);
	self.layerObject = ko.observableArray([]);
    self.layerGroup = ko.observableArray([]);
	self.start = ko.observable(layerJSON.start);
	self.stop = ko.observable(layerJSON.stop);
	self.description = ko.observable(layerJSON.description);
    self.newGroup = ko.observable();
    var selectedGroup = "dd";
    var selectedLayer = "";

	self.newLayer = function(x){
		vectors.removeAllFeatures();
		vectors.events.remove("featureadded");

		if(x != null && typeof(x) == "object"){

        	layerJSON = x;

			console.log(x);
		}
		else
			layerJSON = layerNewEmptyTemplate();

		self.setData();
		vectors.events.on({"featureadded" : function(feature){ layerModel.newLayerObject(feature);}});

		$(".editorView").hide();
		$("#layerEditor").show();
		$("#openlayers_map").addClass("adminMap");

	};

	self.colorPickers = function(el, index, data){
		/*$(el).each(function(){
			console.log($(this).attr('class'));
		});*/
		//console.log("ColorPicker: "+ data.id());
	};

	self.setStart = function( date ){
		self.start(date);
	};

	self.setStop = function( date ){
		self.stop(date);
	};

	self.setData = function(){
		self.description(layerJSON.description);
		self.start(layerJSON.start);
		self.stop(layerJSON.stop);
		self.name(layerJSON.title);
		console.log(layerJSON.data);
		var data  = $.map(layerJSON.data, function(object){

            _.each(object.layers, function(layer){
                var points = [];
                _.each(layer.data, function(data){
                    points.push(new OpenLayers.Geometry.Point(data.x, data.y));
                });
                var linear_ring = new OpenLayers.Geometry.LinearRing(points);
                polygonFeature = new OpenLayers.Feature.Vector(
                    new OpenLayers.Geometry.Polygon([linear_ring]), null, null);
                polygonFeature.id = layer.id;
                polygonFeature.style = {fillColor:layer.color, fillOpacity : 0.4, strokeColor : layer.color, strokeWidth : 1, label : layer.title};
                vectors.addFeatures([polygonFeature]);
            });

			return new layerGroup(object);
		});
		vectors.redraw();
		self.layerGroup(data);
		self.id(layerJSON.id);
		$("#layerStart").attr('value', self.start());
		$("#layerStop").attr('value', self.stop());
	};

    self.getGroup = function(){
        return selectedGroup;
    }

    self.getSelectedLayer = function(){
        return selectedLayer;
    }
    self.setGroup = function(group)
    {
        selectedGroup = group;
    }
    self.setSelectedLayer = function(layer){
        selectedLayer = layer;
    }

    self.newLayerGroup = function()
    {
        console.log("ttt");
        self.layerGroup.push( new layerGroup({id:self.id()+"_"+self.newGroup(), title: self.newGroup()}) );
        selectedGroup = self.id()+"_"+self.newGroup();
        console.log("new group: "+selectedGroup);
        self.newGroup('');
    }

	self.newLayerObject = function(feature)
	{
		console.log("feature added");
		var dataPoints = [];
		vectors.getFeatureById(feature.feature.id).style = {fillColor:"#ee9900", fillOpacity : 0.4, strokeColor : "#ee9900", strokeWidth : 1, label : "label"};
		feature.feature.id = self.id()+"_"+new Date().getTime();
		vectors.redraw();
        console.log(selectedGroup);
		_.each(feature.feature.geometry.getVertices(), function(data){ dataPoints.push({"x" : data.x, "y" : data.y});});
        groupSearch(selectedGroup).addLayerObject(new layerObject(
            {"id" : feature.feature.id,
                "title" : "labele",
                "color" : "#ee9900",
                "data"  : dataPoints,
                "start" : 0,
                "stop"  : 1
            }));
        //self.layerObject.push(new layerObject({"id" : feature.feature.id, "title" : "label", "color" : "#ee9900", "data" : dataPoints}));
	};

	var objectSearch = function(id){
		return ko.utils.arrayFirst(groupSearch(selectedGroup).layers(), function(object){
			return object.id() === id;
		});
	};

    var groupSearch = function(id){
        return ko.utils.arrayFirst(self.layerGroup(), function(object){
            return object.id() == id;
        })
    };

	self.modifyObject = function( id )
	{
		if(curFeatureID != ""){
			var dataPoints = [];
			_.each(vectors.getFeatureById(id).geometry.getVertices(), function(data){ dataPoints.push({"x" : data.x, "y" : data.y});});
			var prevObject = objectSearch(curFeatureID);
			prevObject.data(dataPoints);
		}
		curFeatureID = id;
		var object = objectSearch(id);

        console.log(id);
		console.log(object.title());
	};

	self.removeGroup = function()
	{
        _.each(this.layers(), function(layer){
            vectors.removeFeatures( vectors.getFeatureById(layer.id()) );
        });
        vectors.redraw();
        self.layerObject.remove(this);
		console.log("Remove group " + this.id());

	};

	self.save = function(){
		//vectors.removeAllFeatures();
		//$("#layerEditor").hide();
    	//$("#openlayers_map").removeClass("adminMap");

    	var params = {
			lid : self.id(),
			title : self.name(),
			start : self.start(),
			stop : self.stop(),
			data : ko.toJSON(self.layerGroup()),
			description : self.description()

		};
		var url = (self.id() == -1) ? layerDBCreateUrl : layerDBUpdateUrl;
   		console.log("Data: "+JSON.stringify(params));
		$.ajax({
			url : url,
			method : "POST",
			data : params,
			success : function(data){
				console.log("<saveLayer> $.ajax applied success function with returned data: "+data);
			}
		});
	};
};

function layerGroup(data)
{
    var self = this;

    self.id = ko.observable(data.id);
    self.title = ko.observable(data.title);
    self.layers = ko.observableArray([]);
    self.tools = ko.observable(false);
    _.each(data.layers, function(object){ self.layers.push( new layerObject(object)); });

    self.addLayerObject = function( layer )
    {
        self.layers.push(layer);
    }

    self.toggleControl = function(value, data)
	{
        layerModel.setGroup(self.id());

        $(".Active").addClass("Deactive").removeClass("Active");
        $("."+value).removeClass("Deactive").addClass("Active");
        self.tools((value == "modify"));
		for(key in layerControls)
		{
			var control = layerControls[key];
			(value == key) ? control.activate() : control.deactivate();
		}

		return true;
	}
    self.removeObject = function()
	{
		vectors.removeFeatures( vectors.getFeatureById(this.id()) );
		vectors.redraw();
		self.layers.remove(this);
	};
    self.layerDates = function(){
        $(".layerDate").datetimepicker({
			format : 'dd.mm.yyyy hh:ii:ss',
			autoclose : true,
            pickerPosition: 'bottom-left'
		});
    };
}

function layerObject( data )
{
	this.id = ko.observable(data.id);
	this.title = ko.observable(data.title);
	this.color = ko.observable(data.color);
    this.start = ko.observable(data.start);
    this.stop = ko.observable(data.stop);
    this.start.co = ko.computed({
        read: function(){
            return this.start();
        },
        write: function(value){
            this.start(value);
            if( value < layerModel.start() )
                layerModel.start(value);
        },
        owner: this
    });
    this.stop.co = ko.computed({
        read: function(){
            return this.stop();
        },
        write: function(value){
            this.stop(value);
            if( value > layerModel.stop() )
                layerModel.stop(value);
        },
        owner: this
    });
	this.title.co = ko.computed({
		read : function(){
			return this.title();
		},
		write : function(value)
		{
			this.title(value);
			vectors.getFeatureById(this.id()).style.label = value;
			vectors.redraw();
		},
		owner : this
	});
	this.color.co = ko.computed({
		read : function(){
			return this.color();
		},
		write : function(color){
			this.color(color);
			vectors.getFeatureById(this.id()).style.fillColor = color;
			vectors.getFeatureById(this.id()).style.strokeColor = color;
			vectors.redraw();
		},
		owner : this
	});
    self.toggleModify = function(value, data)
	{
		if( layerModel.getSelectedLayer() != "" )
            layerControls.modify.unselectFeature(layerModel.getSelectedLayer());

        layerModel.setSelectedLayer(vectors.getFeatureById(this.id()));
        layerControls.modify.mode = OpenLayers.Control.ModifyFeature[value.toUpperCase()];
        layerControls.modify.selectFeature(vectors.getFeatureById(this.id()));

		return true;
	};
	var points = [];
	_.each(data.data, function(point){ points.push({"x":point.x, "y":point.y});});
	this.data = ko.observableArray(points);
}

function layerTemplateRender(data){

    data.render_big_date_string = function(){
        return function(text, render){
            return string_from_bigdate(parseInt(render(text)));
        }
    }

    var template = '<tr>' +
        '<td>Layer</td>' +
        '<td></td>' +
        '<td>{{ title }}</td>' +
        '<td>{{ description }}</td>' +
        '<td><button class="btn btn-primary" id="selSelect{{ id }}" style="margin-bottom: 6px;margin-right:10px;">Select</button></td>' +
        '<td></td>' +
        '<td>{{#render_big_date_string}}{{ start }}{{/render_big_date_string}}</td>' +
        '<td>{{#render_big_date_string}}{{ stop }}{{/render_big_date_string}}</td>' +
        '<td></td>' +
        '</tr>';

    return Mustache.render(template,data);
}