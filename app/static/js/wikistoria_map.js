var map, markers, earth;
var dragFeature, point, click, vectors, eventModel, unitModel, layerModel, layerControls, userModel, bazier;
var unit_label_layer = null;
var featureId = 0;
var editedEvent = null;
var curFeatureID = "";
var eventCache = {};
var selectedPlaylist = null;
var selectCntl = null;
var selectedCategories = null;

/*
Parse a Date object from a datetime string
 */
function parse_datetime_object_wikistoria(input){
    if (typeof(input) == "string")
        return Date.parseExact(input,["dd.MM.yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss"]);
    return new Date();
}

/*
Parse a datetime string from Wikistoria database models.
 */
function parse_datetime_string_wikistoria(input){
    if (typeof(input) == "object")
        return input.toString("dd.MM.yyyy HH:mm:ss");

    if (typeof(input) == "string")
        return parse_datetime_object_wikistoria(input).toString("dd.MM.yyyy HH:mm:ss");

    return "10.10.2010 10:10:10"
}

/*
Install an example openlayers map
 */
function installMap(){

    earth = new OpenLayers.Layer.XYZ(
        "Natural Earth",
        [
            "http://a.tiles.mapbox.com/v3/mapbox.natural-earth-hypso-bathy/${z}/${x}/${y}.png",
            "http://b.tiles.mapbox.com/v3/mapbox.natural-earth-hypso-bathy/${z}/${x}/${y}.png",
            "http://c.tiles.mapbox.com/v3/mapbox.natural-earth-hypso-bathy/${z}/${x}/${y}.png",
            "http://d.tiles.mapbox.com/v3/mapbox.natural-earth-hypso-bathy/${z}/${x}/${y}.png"
        ], {
        }
    );

    markers = new OpenLayers.Layer.Markers( "Markers" );

    map = new OpenLayers.Map({
        div: "openlayers_map",
        controls: [
            new OpenLayers.Control.Navigation({
                dragPanOptions: {
                    enableKinetic: true
                }
            }),
            new OpenLayers.Control.PanZoom(),
            new OpenLayers.Control.Attribution()
        ]
    });

    map.addLayer(earth);
    map.addLayer(markers);

    map.addControl(new OpenLayers.Control.LayerSwitcher());

    //map.zoomToMaxExtent();
    map.zoomTo(4.0);

    map.events.register("zoomend", map, function(){
        $("#ui_current_zoom_level").html("Zoom level: "+map.getZoom());
    });

}

// Initialize code
function initializeComponents(){

	// Knockout model creation
    eventModel = new EventViewModel();
    unitModel = new UnitViewModel();
    layerModel = new LayerViewModel();
    userModel = new UserViewModel();

    // Initialize the big date selectors

    // The current time within a big date selector.
    $("#current_bigdate_selector").Bigdate_Selector({
        datedisplay: false,
        captions: false
    });

    $("#anything_Search_startDate").Bigdate_Selector({
        datedisplay: false,
        captions: false,
        initial_current: 0
    });

    $("#anything_Search_endDate").Bigdate_Selector({
        datedisplay: false,
        captions: false,
        initial_current: 930000000000
    });

    $("#eUnit_appearsDatetime").Bigdate_Selector({
        datedisplay: true,
        captions: false,
        change_callback: function(x){
            if (x > unitModel.disappears()){
                unitModel.disappears(x);
            }
            unitModel.appears(x);
        }
    });

    $("#eUnit_disappearsDatetime").Bigdate_Selector({
        datedisplay: true,
        captions: false,
        change_callback: function(x){
            if (x < unitModel.appears()){
                unitModel.appears(x);
            }
            unitModel.disappears(x);
        }
    });

    $("#eventIEditor_startDatetime_test").Bigdate_Selector({
        datedisplay: true,
        captions: false,
        change_callback: function(x){
            if (x > eventModel.end()){
                eventModel.end(x);
            }
            eventModel.start(x);
        }
    });

    $("#eventIEditor_endDatetime_test").Bigdate_Selector({
        datedisplay: true,
        captions: false,
        change_callback: function(x){
            if (x < eventModel.start()){
                eventModel.start(x);
            }
            eventModel.end(x);
        }
    });

	$("#layerStart").datetimepicker({
		format : 'dd.mm.yyyy hh:ii:ss',
		autoclose : true,
        keyboardNavigation : true
	}).on('changeDate', function(ev){
		layerModel.setStart(new Date(ev.date).toString("dd.MM.yyyy HH:mm"));
	});
	
	$("#layerStop").datetimepicker({
		format : 'dd.mm.yyyy hh:ii:ss',
		autoclose : true,
        keyboardNavigation : true
	}).on('changeDate', function(ev){
		layerModel.setStop(new Date(ev.date).toString("dd.MM.yyyy HH:mm"));
	});

    // Bind unit finding window opening click
    // remove _test from name for modal window
    $("#btnUnitFinder_test").click(function(){
        combinedSearch(['playlist','event','layer'],function(X){
            eventEditor_addSelectedUnit(X);
        });
    });
	
	$("#eInfo_Edit").click(function(){
		eventModel.getData();
	});
	
    // Bind event searching in playlist editor
    $("#btnEventFinderForPlaylist").click(event_SearchForPlaylisteditor);
    
    $("#unitIEdit").click(function(){
    	unitEdit();
    });
    
    $("#unitUSave").click(function(){
    	unitModel.unitSave();
    });
    
    $("#unitUDelete").click(function(){
    	unitModel.unitDelete();
    });
    
    $("#eInfo_Delete").click(function(){
    	timeline.deleteItem(editedEvent);
    	eventModel.deleteEvent();
    });
    
    var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;

    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

    vectors = new OpenLayers.Layer.Vector("Vector Layer", {
        renderers: renderer
    });
    bazier = new OpenLayers.Layer.Vector("Bazier routes", {
        renderers: renderer
    });
	
	layerControls = {
		point:      new OpenLayers.Control.DrawFeature(vectors, OpenLayers.Handler.Point),
    	line:       new OpenLayers.Control.DrawFeature(vectors, OpenLayers.Handler.Path),
    	polygon:    new OpenLayers.Control.DrawFeature(vectors, OpenLayers.Handler.Polygon),
    	regular:    new OpenLayers.Control.DrawFeature(vectors, OpenLayers.Handler.RegularPolygon,{handlerOptions: {sides: 5}}),
    	modify:     new OpenLayers.Control.ModifyFeature(vectors, {
            standalone: true,
            beforeSelectFeature: function(	feature	){ layerModel.modifyObject( feature.id );}
    	})
	};
	
	dragFeature = new OpenLayers.Control.DragFeature(vectors,{
        onComplete: function(feature, pixel){
            var e = new OpenLayers.Pixel(pixel.x, pixel.y);
            var lonlat = map.getLonLatFromPixel(e);
            var lat = lonlat.lat;
            var lon = lonlat.lon;
            var f = feature.id.split("_");
            var id = f[2];
            $("#route_lon_"+id).text(lon).change();
            $("#route_lat_"+id).text(lat).change();

            vectors.getFeatureById("label_id_"+id).move(e);

            console.log("Stopped dragging");
        }
    });

	click = new OpenLayers.Control.Click();
	point = new OpenLayers.Control.DrawFeature(vectors, OpenLayers.Handler.Point);
	
	$("#eventEditor").hide();
    $(".unitRoute").hide();
    $("#layerEditor").hide();
        
    map.addLayer(vectors);
    map.addLayer(bazier);
	map.addControl(click);
	map.addControl(point);
	map.addControl(dragFeature);

	// change
	for(key in layerControls){
		map.addControl(layerControls[key]);
	}

    $('ul.nav-tabs').tab();

    $(".userModel").each(function(){
    	ko.applyBindings(userModel, this);
    });

    $(".eventModel").each(function(){
    	ko.applyBindings(eventModel, this);
    });

    $(".unitModel").each(function(){
    	ko.applyBindings(unitModel, this);
    });
    
    $(".layerModel").each(function(){
    	ko.applyBindings(layerModel, this);
    });

    $("#PasswordRetrievalForm_btn").click(function(e){
        // Retrieve a new password
        var email = $("#PasswordRetrievalForm_email").val();
        var username = $("#PasswordRetrievalForm_username").val();
        acquireNewPassword(username,email);
    });

    $(".PasswordRetrievalForm_input").click(function(e){
        e.stopPropagation();
    });

    // Initialize category searching in event search dialog
    categoryFinder(
        $("#anything_Search_category"),
        false,
        $("#anything_Search_findCategories_results"),
        $("#anything_Search_findCategories_selected"),
        null,
        null
    );

    categoryFinder(
        $("#eEditor_CatFindText"),
        true,
        $("#eEditor_FoundCats"),
        $("#eEditor_SelCats"),
        updateEventViewModelForCategories,
        updateEventViewModelForCategories
    );

    // Link for opening more detailed search window
    $(".more-detailed-search").hide();
    $("#anything_Search_MoreDetailedSearchButton").click(function(){
        $(".more-detailed-search").toggle();
    });

    // Enter fires the search
    $("#anything_Search_name").keyup(function(e){
        if (e.keyCode == 13)
            $("#anything_Search_GO").click();
    });

    // Register global keybindings
    $(document).keyup(function(e){

        // Ctrl + i
        if (e.keyCode==73 && e.altKey && e.ctrlKey){
            $("#speedSelector :selected").next().prop("selected", true);
        }

        // Ctrl + k
        if (e.keyCode==75 && e.altKey && e.ctrlKey){
            $("#speedSelector :selected").prev().prop("selected", true);
        }


        // Ctrl + space
        if (e.keyCode==32 && e.ctrlKey && e.ctrlKey){
            toggleAnimation();
        }

        e.preventDefault();

    });

    // Image upload functions for unit editor, unit icon, event icon and playlist icon
    _.each(['north','east','south','west','north_west','north_east','south_west','south_east'],function(dir){

        $('#eUnit_movementImage_'+dir+'_image_form').delegate('#unitMovementImage_'+dir, 'change', function(){
            $('#eUnit_movementImage_'+dir+'_image_form').ajaxForm({
                success: function(data){
                    $("#eUnit_movementImage_"+dir).val(data);
                    unitModel.imageData()[dir] = data;
                    unitModel.imageData(unitModel.imageData());
                }
            }).submit();
        });

    });

    $('#eUnit_movementImage_filename_form').delegate('#unitMovementImage_filename', 'change', function(){
        $('#eUnit_movementImage_filename_form').ajaxForm({
            success: function(data){
                $("#eUnit_Filename").val(data);
                unitModel.filename(data);
            }
        }).submit();
    });

    $('#eventImg_form').delegate('#unitMovementImage_eventImg', 'change', function(){
        $('#eventImg_form').ajaxForm({
            success: function(data){
                $("#eEditor_Filename_test").val(data);
                eventModel.filename(data);
            }
        }).submit();
    });

    $('#playlistIcon_form').delegate('#unitMovementImage_playlist', 'change', function(){
        $('#playlistIcon_form').ajaxForm({
            success: function(data){
                $("#pEditor_Imageurl").val(data);
                $("#playlistIcon_img").attr("src",data);
            }
        }).submit();
    });

    $("#eventIEditor_CooperationEditor").click(function(){
        open_collaborator_editor(eventModel.id(),0);
    });

}

// Knockout data models
var eventJSON = eventNewEmptyTemplate();
var unitJSON = unitNewEmptyTemplate();
var layerJSON = layerNewEmptyTemplate();

ko.bindingHandlers.raty = {
   init: function (element, valueAccessor, allBindingsAccessor) {
      var options = allBindingsAccessor().ratyOptions || {path:'static/img/', number: 10, hints:[
                'Event of insignificant consequences',
                'Event of personal consequences',
                'Event of rural or important personal consequences',
                'Event of communal or important rural consequences',
                'Event of national or important communal consequences',
                'Event of local or important national consequences',
                'Event of regional or important local consequences',
                'Event of global or important regional consequences',
                'Event of important global consequences',
                'Event of crucial global consequences'
            ]};

      var value = ko.utils.unwrapObservable(valueAccessor());
      $(element).raty(options);
      $(element).raty("score", value);

      ko.utils.registerEventHandler(element, "click", function () {
          var observable = valueAccessor();
          observable($(element).raty("score"));
      });
    },
    update : function(element, valueAccessor, allbindingsAccessor){
    	var value = ko.utils.unwrapObservable(valueAccessor());
    	$(element).raty("score", value);
    }
};

ko.bindingHandlers.picker = {
	init : function(element, valueAccessor, allBindingsAccessor){

		$(element).ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			},
			onChange : function(hsb, hex, rgb, el){
				$(el).val("#"+hex).change();
			}
		}).bind('keyup', function(){
			$(this).ColorPickerSetColor(this.value);
		});
	},
	update : function(element, valueAccessor, allbindingsAccessor){
		var value = ko.utils.unwrapObservable(valueAccessor());
		$(element).ColorPickerSetColor(value);
	}
};

/* end knockout */



/*
Update current map marker view
 */
function updateMarkerview(moment){
    // If we have a string, parse date/time out of it
    if (moment == undefined || moment == null){
        moment = current_time();
    } else {
        set_current_time(moment);
    }

    var updated_markers = markerCoordinates(moment);

    markers.clearMarkers();

    if (updated_markers !== null){

        var size = new OpenLayers.Size(27,27);

        _.each(updated_markers, function(x){

            if (x != null){

                // See if we are within zoom levels
                var zoomLevel = map.getZoom();

                if (((parseInt(x[4].zoom_min) || 0) <= zoomLevel) && (zoomLevel <= (parseInt(x[4].zoom_max || 20)))){

                    // Select movement unit icon with many fallback options
                    var unitIcon = _.first(_.compact([x[2].imageData[x[3][0]],x[2].imageData[x[3][1]],x[2].imageData['north'],x[2].filename]));

                    // Unit label
                    var unitLabel = x[4].unit_label;

                    // Select a marker for an unit
                    var tmpMarker = new OpenLayers.Marker(
                        new OpenLayers.LonLat(x[0],x[1]),
                        new OpenLayers.Icon(unitIcon, size)
                    );

                    var unitLabelElement = $(tmpMarker.icon.imageDiv);
                    var unitLabelElement_span = $("<span>"+unitLabel+"</span>");

                    unitLabelElement_span.css({
                        "color" : "black",
                        "text-shadow" : "silver 1px 1px"
                    });

                    unitLabelElement.append("<br/>");
                    unitLabelElement.append(unitLabelElement_span);

                    markers.addMarker(tmpMarker);

                    unitLabelElement.css("width","150px");

                }
            }

        });

    }

}