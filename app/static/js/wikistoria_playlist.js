function playlist_SavePlaylist(){
    var url = (selectedPlaylist.id == -1) ? playlistDBCreateUrl : playlistDBUpdateUrl;

    var innerData = ko.toJSON({
        IDs : _.pluck(selectedPlaylist.data,"id")
    });

    var data = {
        id : selectedPlaylist.id,
        title : $("#pEditor_Title").val(),
        description : $("#pEditor_Description").val(),
        filename : $("#pEditor_Imageurl").val(),
        data: innerData
    };

    $.ajax({
        url : url,
        data : data,
        method : "POST",
        success : function(data){
            console.log("<playlist_SavePlaylist> Ajax returned success data: "+data);
        }
    });
}

function playlist_OpenEditor(x){
    selectedPlaylist = (x != undefined && x != null) ? x : playlist_EmptyJSONTemplate();
    $("#pEditor_Title").val(selectedPlaylist.title);
    $("#pEditor_Imageurl").val(selectedPlaylist.filename);
    $("#pEditor_Description").text(selectedPlaylist.description);

    // Check if the object was loaded from server
    if (_.has(selectedPlaylist.data,"IDs")){
        var tmp = new listenerArray(playlist_EditorRenderEvents);
        _.each(selectedPlaylist.data.IDs, function(x){
            tmp.push(
                loadAndReturnSingleEventByID(x)
            );
        });
        selectedPlaylist.data = tmp;
    }

    playlist_EditorRenderEvents();

    var saveButton = $("#playlistIEditor_btnSave");
    saveButton.unbind("click").click(function(){
        playlist_SavePlaylist();
    });

    var exportButton = $("#pEdit_Export");
    exportButton.attr("href", "playlists/export/json/"+ selectedPlaylist.id);

    if (selectedPlaylist.id == -1){
        saveButton.text("Create");
        exportButton.hide();
    } else {
        saveButton.text("Update");
        exportButton.show()
    }

    $("#playlistIEditor_Editor").modal();
}

function promptPlaylistRemoval(pID){
    bootbox.confirm("Confirm playlist removal", function(result) {
        if (result){
            removeSinglePlaylist(pID);
        }
    });
}

function removeSinglePlaylist(pID){
    var playlistObject = _.find(currentlyLoadedPlaylists,function(x){ return x.id == pID; });
    if (playlistObject){
        _.each(playlistObject.data.IDs, function(eventID){
            currentlyLoadedEvents.removeById(eventID);
        });
        currentlyLoadedPlaylists.removeById(playlistObject.id);
        $("#eventTab_Eventlist_Playlist_Header_"+playlistObject.id).remove();
        $("#eventTab_Eventlist_Playlist_"+playlistObject.id).remove();
        setupDataForCoordinates();
        $('#eventContentTabs_Headers a:first').tab('show');
    }
}

function import_playlist(){
    bootbox.prompt("Copy and paste the playlist data here", function(result){
        if (result && result.length > 10){

            playlist_object = JSON.parse(result);
            loadPlaylist(playlist_object);

            $.notifyBar({
                html: "Playlist '"+ playlist_object.title +"' loaded!",
                delay: 2000,
                animationSpeed: "normal",
                position: "bottom"
            });

        }
    });
}

function loadPlaylist(X){
    // Check if the data.IDs exists? If no, then create it
    if (!_.has(X.data,"IDs")){
        X.data.IDs = _.pluck(X.data,"id");
    }

    if (currentlyLoadedPlaylists.pushUnique(X)){

        var nextTab = X.id;
        $('<li id="eventTab_Eventlist_Playlist_Header_'+nextTab+'"><a href="#eventTab_Eventlist_Playlist_'+nextTab+'" data-toggle="tab">'+ X.title +' <button class="btn" style="padding:0;"><i onclick="promptPlaylistRemoval('+X.id+');" class="icon-remove"></i></button></a></li>').appendTo('#eventContentTabs_Headers');
        $('<div class="wikistoria-event-tab tab-pane" id="eventTab_Eventlist_Playlist_'+nextTab+'"><h4>'+ X.title +'</h4></div>').appendTo('#eventContentTabs_Containers');
        $('#eventContentTabs_Headers a:last').tab('show');

        _.each(X.data.IDs, function(value){
            // loadSingleEvent is able to register a callback for loaded events. Because the
            // events are loaded asynchronously, the callback is needed.
            loadSingleEvent(value, null, function(eventObject){
                $("#eventTab_Eventlist_Playlist_"+nextTab).append(event_renderSelectionAnchor(eventObject,false));
            });
        });

    }
}

function renderPlaylistTemplate(data){

    var template = '<tr>' +
        '<td>Playlist</td>' +
        '<td><img src="{{ filename }}" style="max-height: 50px;max-width: 50px;" /></td>' +
        '<td>{{ title }}</td>' +
        '<td>{{ description }}</td>' +
        '<td><button class="btn btn-primary" id="selSelect{{ id }}" style="margin-bottom: 6px;margin-right:10px;">Select</button></td>' +
        '<td></td>' +
        '<td></td>' +
        '<td></td>' +
        '<td></td>' +
        '</tr>';

    return Mustache.render(template,data);
}

// An implementation of a simple observer pattern for an array
var listenerArray = function(addCallback,removeCallback,addOrRemoveCallback) {
    var array_ = [];

    // Override the default pushing method, in order to implement listener for data
    array_.push = function() {

        // The original pushing method
        Array.prototype.push.apply(this, arguments);

        // If we have a registered callback, call it in the right context
        if (addCallback != undefined && addCallback != null)
            addCallback(arguments[0]);

        // By definition, push returns the new array length
        return this.length;

    };

    // Extension of default push, append only if new element isn't in array
    array_.pushUnique = function() {

        // If the argument is an object, and it has ID property, then test for the existing ID values
        if (typeof(arguments[0]) == "object" && _.has(arguments[0],'id')){
            var old_IDs = _.pluck(this,'id');
            if (_.contains(old_IDs,arguments[0].id)){
                console.log("<listenerArray> Tried to add object with existing duplicate ID, ignoring");
                return false;
            }
        }

        if (this.indexOf(arguments[0]) == -1){

            // The original pushing method
            Array.prototype.push.apply(this, arguments);

            // If we have a registered callback, call it in the right context
            if (addCallback != undefined && addCallback != null)
                addCallback(arguments[0]);

            if (addOrRemoveCallback != undefined && addOrRemoveCallback != null)
                addOrRemoveCallback(this);

        }
        // By definition, push returns the new array length
        return this.length;
    };

    // Some own boilerplate code
    array_.removeById = function(){
        var old_IDs = _.pluck(this,'id');
        if (_.contains(old_IDs,arguments[0])){
            this.splice(old_IDs.indexOf(arguments[0]),1);
        }
        if (removeCallback != undefined && removeCallback != null)
            removeCallback();
        if (addOrRemoveCallback != undefined && addOrRemoveCallback != null)
            addOrRemoveCallback(this);
    };

    array_.findIndexById = function(){
        return _.indexOf(_.pluck(this,'id'),arguments[0]);
    };

    array_.clear = function(){
        while (this.length)
            this.pop();
        if (removeCallback != undefined && removeCallback != null)
            removeCallback();
        if (addOrRemoveCallback != undefined && addOrRemoveCallback != null)
            addOrRemoveCallback(this);
    };

    array_.hasItem = function() {
        return this.indexOf(arguments[0]) > 0;
    };

    array_.hasItems = function() {
        var args = Array.prototype.slice.call(arguments);
        return _.every(args,this.hasItem,this);
    };

    return array_;
};

function playlist_EmptyJSONTemplate(){
    return {
        id: -1,
        // data is an array containing event JSON objects
        data: new listenerArray(null,null,playlist_EditorRenderEvents),
        author: logged_id,
        title: '',
        description: '',
        filename: '',
        created: parse_datetime_string_wikistoria(new Date()),
        edited: parse_datetime_string_wikistoria(new Date())
    };
}

function playlist_EditorRenderEvents(){
    var cont = $("#pEditor_SelectedEvents");
    cont.html('');

    if (selectedPlaylist != null && selectedPlaylist.data.length > 0){
        _.each(selectedPlaylist.data, function(X){
            cont.append(
                event_renderSmallInformation(X,true)
            );
        });
    }
}