function renderFindResults(data, renderFunc, selectFunc1){
    return _.map(data,function(U){
        // Render a template for event information

        // Check if there is a need for rendering select button
        U.selJavascript = '';
        if (selectFunc1 != undefined && typeof(selectFunc1) == "function"){
            U.selJavascript = 'yes';
        }

        if (_.has(U,'significance'))
            U.repeatSignificanceStars = new Array(U.significance+1).join('<i class="icon-star"></i>');

        // Render the template
        var renderedTemplate = renderFunc(U);

        // Make a jQuery element out of the rendered template
        var UEl = $(renderedTemplate);

        UEl.first("tr td#selSelect"+ U.id).click(function(){
            selectFunc1(U);
        });

        return UEl;

    });
}

/*
Search content, return Ajax call for chaining requests in Promise-pattern.
 */
function find_Units(filterModel){
    if (_.contains(filterModel,"unit"))
        return false;

    var searchText = $("#anything_Search_name").val();

    // Threaded Ajax-call for searching events
    return $.ajax({
        url: unitDBSearchUrl,
        data: {searchText:searchText},
        dataType: "json",
        type: "GET"
    });
}

/*
Search content, return Ajax call for chaining requests in Promise-pattern.
 */
function find_Events(filterModel){
    if (_.contains(filterModel,"event"))
        return false;

    var searchText = $("#anything_Search_name").val();

    var searchData = {
        howmany:10,
        startdate:$("#anything_Search_startDate").Bigdate_Selector().getdate_seconds(),
        enddate:$("#anything_Search_endDate").Bigdate_Selector().getdate_seconds(),
        searchtext:searchText,
        maxsignificance:$("#anything_Search_sigEnd").val(),
        minsignificance:$("#anything_Search_sigStart").val(),
        categories:_.pluck(selectedCategories,'id').join()
    };

    // Threaded Ajax-call for searching events
    return $.ajax({
        url: eventDBQueryURL,
        data: searchData,
        dataType: "json",
        type: "GET"
    });
}

/*
Search content, return Ajax call for chaining requests in Promise-pattern.
 */
function find_Playlists(filterModel){
    if (_.contains(filterModel,"playlist"))
        return false;

    // Threaded Ajax-call for searching playlist
    var searchText = $("#anything_Search_name").val();

    return $.ajax({
        url: playlistDBSearchUrl,
        data: {searchText:searchText},
        dataType: "json",
        type: "GET"
    });
}

/*
Search content, return Ajax call for chaining requests in Promise-pattern.
 */
function find_Layers(filterModel){
    if (_.contains(filterModel,"layer"))
        return false;

	var searchText = $("#anything_Search_name").val();

	var searchData = {
		howmany:10,
		searchtext : searchText
	};

	return $.ajax({
		url : layerDBSearchUrl,
		data : searchData,
		dataType : "json",
		type : "GET"
	});

}

/*
General functionality for searching different types of content.
 */
function combinedSearch(filterModel,selFunction){

    // Check if we are in the main search mode
    if (filterModel == undefined){
        filterModel = [];
    }

    var resultContainer = $("#anything_Search_results");
    resultContainer.html('');

    $("#anything_Search_GO").unbind('click').click(function(){

        if ($("#anything_Search_name").val().length > 2){


            // Check for main search mode filters, and whether we are searching for a single type of content
            if ($("#anything_Search_checkbox_event").length > 0 && filterModel.length != 3){

                filterModel = _.map(['event','unit','playlist','layer'], function(x){
                    return (!$("#anything_Search_checkbox_"+x).prop("checked")) ? x : '';
                });

            }


            $.when(find_Events(filterModel),find_Units(filterModel),find_Playlists(filterModel),find_Layers(filterModel)).done(function(a,b,c,d){

                resultContainer.html('');

                var baseTable = $('<table class="table-condensed table-striped"><thead style="font-weight: bold;"><tr><td>Type</td><td>Image</td><td>Name</td><td>Description</td><td>Select</td><td>Significance</td><td>Start date</td><td>End date</td><td>Incertitude</td></tr></thead><tbody></tbody></table>');

                baseTable.first("table tbody").append(
                    renderFindResults(a[0],renderEventTemplate, (selFunction != undefined) ? selFunction : loadSingleEvent ),
                    renderFindResults(b[0],renderEventUnitTemplate, (selFunction != undefined) ? selFunction : unitInfo ),
                    renderFindResults(c[0],renderPlaylistTemplate, (selFunction != undefined) ? selFunction : loadPlaylist ),
                    renderFindResults(d[0],layerTemplateRender, (selFunction != undefined) ? selFunction : layerModel.newLayer )
                );

                resultContainer.append(baseTable);

            });

        }

    });

    selectedCategories.clear();
    $("#anything_Search_findCategories_selected").html('');
    $("#anything_Search").modal();

}