var timeline;
var timeline_container;
var movement_data = null;
var unit_data = {};
var animating = false;
var animateTimeout = null;

function current_time(){
    return $("#current_bigdate_selector").Bigdate_Selector().getdate_seconds();
}

function current_time_comps(){
    return $("#current_bigdate_selector").Bigdate_Selector().getdate_components();
}

function set_current_time(t){
    $("#current_bigdate_selector").Bigdate_Selector().setdate_seconds(t);
}

/*
Setup the event unit marker and coordinate data for continuous polling
 */
function setupDataForCoordinates(){

    // Filter empty data away
    var datas = _.filter(currentlyLoadedEvents, function(x){ return x && x.data != undefined && x.data.length > 0 });

    // Extract all single unit data and save the event start, end time and event id
    this.movement_data = _.flatten(
        _.map(datas,
            function(X){
                return _.map(X.data,function(X2){
                    X2.start = X.start;
                    X2.end = X.end;
                    X2.eventID = X.id;
                    return X2;
                })
            }
        )
    );

    // Extract and fetch used unit information
    var needed_units = _.uniq(_.pluck(this.movement_data,'eventunit'));
    fetchNeededUnits(needed_units);
}

function dirPictures(dx,dy){
    var fst = "north", snd = "north";

    if (dx > 0 && dy < 0){
        fst = "south_east";
        snd = (dx > (dy*-1)) ? "east" : "south";
    }

    if (dx < 0 && dy < 0){
        fst = "south_west";
        snd = ((dx*-1) > (-1*dy)) ? "west" : "south";
    }

    if (dx > 0 && dy > 0){
        fst = "north_east";
        snd = (dx > dy) ? "east" : "north";
    }

    if (dx < 0 && dy > 0){
        fst = "north_west";
        snd = ((dx*-1) > dy) ? "west" : "north";
    }

    var diagonal_movement_threshold = 0.5;

    if (Math.abs(dx) < diagonal_movement_threshold && dy < 0){
        fst = "south";
        snd = "south";
    }

    if (Math.abs(dx) < diagonal_movement_threshold && dy > 0){
        fst = "north";
        snd = "north";
    }

    if (dx > 0 && Math.abs(dy) < diagonal_movement_threshold){
        fst = "east";
        snd = "east";
    }

    if (dx < 0 && Math.abs(dy) < diagonal_movement_threshold){
        fst = "west";
        snd = "west";
    }

    return [fst,snd]
}

/*
Find coordinates for different units inside selected events for a given moment using simple interpolation
on the clients side. Events and data is accessible at currentlyLoadedEvents.
 */
function markerCoordinates(moment){

    if (!this.movement_data){
        return [];
    }

    // Go through the pre-processed movement data, try to find current coordinates for unit markers
    return _.map(this.movement_data, function(x){

        var dx = 0, dy = 0;

        // Lets check if the current moment is between unit movement information
        var timeComp = _.map(x.datapoints, function(x2){ return compare_bigdates(moment,x2.time); });

        var uniqCheck = _.uniq(timeComp);

        // If we have only one unique -1 value, then the movement event hasn't started yet, and we will
        // return the initial position if the event itself has started already
        if (uniqCheck.length == 1 && uniqCheck[0] == -1){

            // Check if event has started but if an unit hasn't started to move yet
            if (compare_bigdates(moment,x.start) == 1){
                return [x.datapoints[0].x,x.datapoints[0].y,unit_data[x.eventunit],dirPictures(dx,dy),x];
            }

            return null;

        }

        // If we have only one unique 1 value, then the movement event has started and is finished. Now we will
        // return the resulting final location if the event itself has not stopped yet
        if (uniqCheck.length == 1 && uniqCheck[0] == 1){

            // Check if event has not ended but if an unit has stopped to moving
            if (compare_bigdates(moment,x.end) == -1){
                return [x.datapoints[x.datapoints.length-1].x,x.datapoints[x.datapoints.length-1].y,unit_data[x.eventunit],dirPictures(dx,dy),x];
            }

            return null;
        }

        // Now we will interpolate the correct location
        if (uniqCheck.length > 1){

            // Lets find where the date is
            var startIndex = timeComp.indexOf(-1)-1;
            var endIndex = startIndex + 1;

            // Some basic validation and we are ready to go
            if (startIndex >= 0 && endIndex < timeComp.length){

                var startDate = x.datapoints[startIndex].time;
                var endDate = x.datapoints[endIndex].time;

                var startX = x.datapoints[startIndex].x;
                var startY = x.datapoints[startIndex].y;

                var endX = x.datapoints[endIndex].x;
                var endY = x.datapoints[endIndex].y;

                var posInterpol = (moment - startDate) / (endDate - startDate);

                dx = ((endX - startX) * posInterpol);
                dy = ((endY - startY) * posInterpol);

                var curX = startX + dx;
                var curY = startY + dy;

                return [curX,curY,unit_data[x.eventunit],dirPictures(dx,dy),x];

            }

        }

        // Something went wrong if we return at this point
        if (x.datapoints.length == 0)
            return null;
        else
            return [x.datapoints[0].x,x.datapoints[0].y,unit_data[x.eventunit],dirPictures(dx,dy),x];

    });

}

function animate() {

    var selSpeed = parseFloat($("#speedSelector").val());

    animateTimeout = setTimeout(animate, 50);

    $("#current_bigdate_selector").Bigdate_Selector().adjust_delta(selSpeed/20);

    updateMarkerview();

}

/*
Start or stop the actual animation
 */
function toggleAnimation(){
    animating = !animating;
    if (animating){

        animate();

    } else {
        clearTimeout(animateTimeout);
        animateTimeout = null;
    }
}