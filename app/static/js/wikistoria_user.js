function render_collaborator_removal_button(username,content_id,content_type,user_id){
    var btn_remove_collaborator = $("<button></button>");
    btn_remove_collaborator.addClass("btn");
    btn_remove_collaborator.html(username);
    btn_remove_collaborator.click(function(){
        var this_ = this;
        $.post(removeCollaboratorUrl, {
            content_id : content_id,
            content_type : content_type,
            user_id : user_id
        }, function(data){
            if (data == "ok")
                $(this_).hide();
        });
    });
    return btn_remove_collaborator;
}

function open_collaborator_editor(resource_id,resource_type){

    // Make sure that the parameters are defined (given)
    if (!_.some([resource_id,resource_type], _.isUndefined)){

        var collaborator_name_container = $("#unitCollaborationEditForm_usernames");

        // Load the current collaborators
        collaborator_name_container.empty();

        $.post(collaboratorUrl, {
            content_id : resource_id,
            content_type : resource_type
        }, function(data){

            if (data.length > 2){

                var data_json = JSON.parse(data);

                _.each(data_json, function(data_){
                    collaborator_name_container.append(render_collaborator_removal_button(
                        data_.username,
                        resource_id,
                        resource_type,
                        data_.id
                    ));
                });

            }

        });

        // Register an appropriate function for adding collaborators
        $("#unitCollaborationEditForm_addbutton").unbind('click').click(function(){

            var collaborator_name = $("#unitCollaborationEditForm_collaborator").val();

            if (collaborator_name.length > 2){
                $.post(setCollaboratorUrl, {
                    content_id : resource_id,
                    content_type : resource_type,
                    collaborator : collaborator_name
                }, function(data){
                    if (data.length > 0){

                        var data_json = JSON.parse(data);

                        collaborator_name_container.append(render_collaborator_removal_button(
                            data_json.username,
                            resource_id,
                            resource_type,
                            data_json.id
                        ));

                    }
                });
            }

        });

        // Show the editor
        $("#unitCollaborationEditForm").modal();

    }

}

var UserViewModel = function()
{
	var self = this;
	this.id = ko.observable(-1);
    this.realname = ko.observable("");
    this.username = ko.observable("");
    this.description = ko.observable("");
    this.registered = ko.observable("");
    this.email = ko.observable("");

    self.settings = function(){
        $("#windowUserSettings").modal();
        self.cacheValues();
    };

    self.cacheValues = function(){
        if (self.id() == -1){
            self.loadLoggedData();
        }
    };

    self.loadLoggedData = function(){
        $.post(loggedUserInfo, {}, function(data){
            data = JSON.parse(data);
            self.id(data.id);
            self.realname(data.realname !== null ? data.realname : "");
            self.username(data.username);
            self.description(data.description);
            self.registered(data.registered);
            self.email(data.email);
        })
    };

    self.updateDatabase = function(){
        // Transform the UserViewModel into a single JSON object
        var data = JSON.parse(ko.toJSON(userModel));
        $.post(loggedUserUpdateInfo, data, function(){

        });
    };

    self.computeFirstname = ko.computed(function(){
        if (self.realname() !== null && self.realname().length > 0)
            return self.realname().split(" ")[0];
        else
            return null;
    }, self);

    self.validEmail = ko.computed(function() {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(self.email());
    }, self);

    self.yourContentGreet = ko.computed(function(){
        var first_name = self.computeFirstname();

        if (first_name !== null)
            return "Hey " + first_name + "! Here is your content";

        return "Your content. What is your name?";
    });

    self.settingsStatusText = ko.computed(function(){
        if (self.realname() === null || self.realname().length == 0)
            return "Hello stranger. What is your name?";

        if (self.realname().length < 3)
            return "Your name is rather short";

        if (self.realname().split(" ").length <= 1)
            return "Do you have a surname "+self.computeFirstname()+"?";

        if (!self.validEmail())
            return self.computeFirstname() + ", your email doesn't seem valid.";

        if (self.description().length < 30)
            return self.computeFirstname() + ", we'd love to hear more about you.";

        return "Hi "+self.realname();
    }, self);

};

function acquireNewPassword(username,email){
    if (username && email && username.length > 2 && email.length > 4){
        $.post(passwordChangeURL, {username:username,email:email}, function(data){

            if (data == "1"){
                $.notifyBar({
                    html: "Password changed! Please check your email.",
                    delay: 3000,
                    animationSpeed: "normal",
                    position: "bottom"
                });
            }

            if (data == "0"){
                $.notifyBar({
                    html: "Error changing password. Please check your username and password.",
                    delay: 3000,
                    animationSpeed: "normal",
                    position: "bottom"
                });
            }

        })
    }
}