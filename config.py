# Wikistoria configuration file

import json
import os

# Wikistoria deployment URL
WIKISTORIA_URL = "http://wikistoria.eu01.aws.af.cm/"

# An environmental variable that contains a JSON document of bound services and
# their credentials in AppFog
services = json.loads(os.getenv("VCAP_SERVICES", "{}"))

SECRET_KEY = 'k4_3&kfa/FMQ?ea'

CSRF_ENABLED = True
CSRF_SESSION_KEY = "m(a_zdnd-/*qSDv###"

# Datetime formats in Python. MySQL uses the iso standard ISO 8601 for datetime format.
DTF_MySQL = "%Y-%m-%d %H:%M:%S"
DTF_UserInterface = "%d.%m.%Y %H:%M:%S"

# Amazon S3 credentials
AWSAccessKeyId = "AKIAJHTYR7EVU2X2DTPQ"
AWSSecretKey = "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"

# Amazon S3 bucket for storing wikistoria images
AWS_S3_Bucket = "wikistoriaimages"

# Mailgun credentials for sending email to users
MAILGUN_Api_Key = "key-1zb09wtclpuhe7d8ql72tckf58xpv0f5"
MAILGUN_HTTP_API_URL = "https://api.mailgun.net/v2/wikistorianiko.mailgun.org/messages"

# Salt is added in passwords before hashing
SALT = "ISOPJsdfQdU)Iw53wzKJLSHERaaD()8e7r9q2038"

# Check if we are in cloud or local development environment. This information
# is used to setup MySQL connection correctly.
if services:
    creds = services['mysql-5.1'][0]['credentials']
    SQLALCHEMY_DATABASE_URI = "mysql://%s:%s@%s:%d/%s" % (
        creds['username'],
        creds['password'],
        creds['hostname'],
        creds['port'],
        creds['name'])
else:
    SQLALCHEMY_DATABASE_URI = 'mysql://wikistoria:wikistoria@localhost/wikistoria'

# Wikistoria content configuration

# Preset collection of public categories in a dictionary. The key is a main category.
preset_public_categories = {
    'social': ['art', 'culture', 'religion', 'religious engineering', 'civil conflict', 'crime', 'urban engineering', 'sport', 'social area', 'insurrection', 'revolution', 'ethnos', 'tribe', 'social network'],
    'science': ['invention', 'biology', 'biogeography', 'disease', 'epidemic', 'wildlife tracking', 'chemistry', 'physics', 'scientific engineering', 'cartography', 'ethnology', 'internet'],
    'politics': ['territory', 'diplomacy', 'intelligence', 'political crime', 'discovery travel', 'colonization', 'nation', 'police'],
    'military': ['military engineering', 'enlistment', 'deployment', 'movement', 'maneuver', 'expedition', 'campaign', 'siege', 'cyberwar', 'war'],
    'economy': ['labour', 'trade', 'production', 'service', 'resource', 'agriculture', 'economic engineering', 'slavery']
}

# Preset collection of private categories in a dictionary. The key is a main category.
preset_private_categories = {
    'private': ['living', 'family', 'hobby', 'music', 'sports', 'friends', 'party', 'travel', 'sight-seeing', 'activity', 'job', 'project', 'business', 'internet'],
    'simulation': ['analysis', 'draft', 'preliminary research result']
}

# Contents of emails for acquiring new passwords
NEWPASSWORD_EMAIL_SUBJECT = "New password for your Wikistoria account"

# Messages in user interface. Variable {0} is automatically filled by Wikistoria
UI_MESSAGES_SUCCESSFUL_LOGIN = "Logged in as user {0}." # {0} = user name of the logged user
UI_MESSAGES_UNSUCCESSFUL_LOGIN = "Incorrect login information."
UI_MESSAGES_ERROR_LOGIN = "Error parsing the login form."
UI_MESSAGES_SUCCESSFUL_REGISTER = "Welcome {0! You are successfully registered, and may login now."
UI_MESSAGES_NAME_TAKEN_REGISTER = "The username {0} is already registered! Please select another one."
UI_MESSAGES_ERROR_REGISTER = "Registration error. Please check the information that you provided."